#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 9.8</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>Stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la huitième mise à jour de sa
distribution stable Debian <release> (nommée <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis
à niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction arc "Correction de bogues de traversée de répertoires [CVE-2015-9275], du plantage d'arcdie quand il est appelé avec plus d'un argument de variable, et de la lecture de l'en-tête d'arc version 1">
<correction astroml-addons "Correction des dépendances à Python 3">
<correction base-files "Mise à jour pour cette version">
<correction c3p0 "Correction d'une vulnérabilité d'injection d'entités externes XML [CVE-2018-20433]">
<correction ca-certificates-java "Correction de la création temporaire de jvm-*.cfg sur armhf">
<correction chkrootkit "Correction de l'expression rationnelle pour le filtrage de dhcpd et dhclient comme faux positifs à partir de tests du renifleur de paquets">
<correction compactheader "Mise à jour pour fonctionner avec les dernières versions de Thunderbird">
<correction courier "Correction des substitutions @piddir@">
<correction cups "Corrections de sécurité [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config "Correction de configuration des pages web personnelles ; réactivation de l'installation hors ligne d'un serveur combiné incluant la prise en charge de stations de travail sans disque ; activation de la configuration de la page d'accueil de Chromium au moment de l'installation et avec LDAP">
<correction debian-installer "Reconstruction pour cette version">
<correction debian-installer-netboot-images "Reconstruction avec les propositions de mises à jour">
<correction debian-security-support "Mise à jour de l'état de prise en charge de divers paquets">
<correction dnspython "Correction d'erreur lors de l'analyse de tableaux de bits nsec3 à partir de textes">
<correction egg "emacsen-install ignoré pour xemacs21 qui n'est pas pris en charge">
<correction erlang "Pas d'installation du mode Erlang pour XEmacs">
<correction espeakup "debian/espeakup.service : correction de la compatibilité avec les versions antérieures de systemd">
<correction freerdp "Correction de problèmes de sécurité [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788] ; ajout de la prise en charge de CredSSP v3 et de RDP proto v6">
<correction ganeti-os-noop "Correction de la détection de taille pour les périphériques qui ne sont pas des périphériques en mode bloc">
<correction glibc "Correction de plusieurs problèmes de sécurité [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237] ; erreurs de segmentation évitées sur les processeurs avec AVX512-F ; correction d'une utilisation de mémoire après libération dans pthread_create() ; recherche de PostgreSQL dans la vérification de NSS ; correction de pthread_cond_wait() dans le cas de pshared sur les machine non x86.">
<correction gnulib "vasnprintf : correction d'un bogue de dépassement de mémoire de tas [CVE-2018-17942]">
<correction gnupg2 "Plantage évité lors de l'importation sans un TTY">
<correction graphite-api "Correction de l'orthographe de RequiresMountsFor dans le service systemd">
<correction grokmirror "Ajout de la dépendance manquante à python-pkg-resources">
<correction gvrng "Correction d'un problème de droits qui empêchait le démarrage de gvrng ; création de dépendances correctes à Python">
<correction ibus "Correction de l'installation multi-architecture en retirant la dépendance à Python du paquet gir">
<correction icinga2 "Correction des estampilles temporelles stockées comme heure locale dans PostgreSQL">
<correction intel-microcode "Ajout de corrections accumulées pour Westmere EP (signature 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort "Correction des dépendances à Python">
<correction jdupes "Correction d'un plantage potentiel sur ARM">
<correction kmodpy "Retrait de l'étiquette « Multi-Arch : same » incorrecte de python-kmodpy">
<correction libapache2-mod-perl2 "Sections &lt;Perl&gt; plus autorisées dans la configuration contrôlée par l'utilisateur [CVE-2011-2767]">
<correction libb2 "Détection de la capacité du système d'utiliser AVX avant son utilisation">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libemail-address-list-perl "Correction d'une vulnérabilité de déni de service [CVE-2018-18898]">
<correction libemail-address-perl "Correction de vulnérabilités de déni de service [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod "python-gpod : ajout de la dépendance manquante à python-gobject-2">
<correction libssh "Correction de l'authentification côté serveur interactive cassée">
<correction linux "Nouvelle publication amont ; nouvelle version amont ; correction d'échecs de construction sur arm64 et mips* ; libceph : correction de la vérification de CEPH_FEATURE_CEPHX_V2 dans calc_signature()">
<correction linux-igd "$network requis par le script d'init">
<correction lttng-modules "Correction de construction avec les noyaux linux-rt 4.9 et les noyaux &gt;= 4.9.0-3">
<correction mistral "Correction de <q>std.ssh action may disclose presence of arbitrary files</q> [CVE-2018-16849]">
<correction monkeysign "Corrections d'un problème de sécurité [CVE-2018-12020] ; envoi de plusieurs courriers électroniques au lieu d'un seul">
<correction mpqc "Installation de sc-libtool également">
<correction nvidia-graphics-drivers "Nouvelle version amont">
<correction nvidia-modprobe "Nouvelle version amont">
<correction nvidia-persistenced "Nouvelle version amont">
<correction nvidia-settings "Nouvelle version amont">
<correction nvidia-xconfig "Nouvelle version amont">
<correction openni2 "Correction de violation de la base commune d'armhf et d'échec de construction depuis le source (« FTBFS ») d'armel provoqué par l'utilisation de NEON">
<correction openvpn "Correction du comportement de NCP à la reconnexion de TLS, provoquant des erreurs <q>AEAD Decrypt error: cipher final failed</q>">
<correction parsedatetime "Ajout de la prise en charge de Python 3">
<correction pdns "Correction de problèmes de sécurité [CVE-2018-1046 CVE-2018-10851] ; correction de requêtes MySQL avec des procédures stockées ; correction des dorsaux LDAP, Lua et OpenDBX ne trouvant pas de domaine">
<correction pdns-recursor "Correction de problèmes de sécurité [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage "Ajout de la dépendance manquante à gir1.2-gtk-3.0">
<correction postfix "Nouvelle version amont stable ; échecs de postconf quand postfix-instance-generator est exécuté pendant le démarrage">
<correction postgresql-9.6 "Nouvelle version amont">
<correction postgrey "Reconstruction sans changement">
<correction pylint-django "Correction des dépendances à Python 3">
<correction python-acme "Rétroportage de la version la plus récente à cause de la dépréciation de tls-sni-01">
<correction python-arpy "Correction des dépendances à Python 3">
<correction python-certbot "Rétroportage de la version la plus récente à cause de la dépréciation de tls-sni-01">
<correction python-certbot-apache "Mise à jour à cause de la dépréciation de tls-sni-01">
<correction python-certbot-nginx "Mise à jour à cause de la dépréciation de tls-sni-01">
<correction python-hypothesis "Correction des dépendances (inverses) à python3-hypothesis et python-hypothesis-doc">
<correction python-josepy "Nouveau paquet, requis par Certbot">
<correction pyzo "Ajout de la dépendance manquante à python3-pkg-resources">
<correction r-cran-readxl "Correction de bogues de plantage [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit "Passage de dbus et polkit de Recommends à Depends">
<correction ruby-rack "Correction d'une possible vulnérabilité de script intersite [CVE-2018-16471]">
<correction samba "Nouvelle version amont ; s3:ntlm_auth : correction de fuite de mémoire dans manage_gensec_request() ; erreurs de démarrage de nmbd ignorées quand il n'y a pas d'interface non loopback ou pas d'interface locale IPv4 non loopback ; correction de la régression CVE-2018-14629 sur un enregistrement non CNAME">
<correction sl-modem "Prise en charge des version de Linux &gt; 3">
<correction sogo-connector "Mise à jour pour fonctionner avec les dernières versions de Thunderbird">
<correction sox "Application effective des corrections pour CVE-2014-8145">
<correction ssh-agent-filter "Correction d'écriture de deux octets hors limite de pile">
<correction supercollider "Désactivation de la prise en charge de XEmacs et Emacs &lt;=23">
<correction sympa "Retrait de /etc/sympa/sympa.conf-smime.in des fichiers de configuration ; utilisation du chemin complet pour la commande head dans le fichier de configuration de Sympa">
<correction twitter-bootstrap3 "Correction de plusieurs vulnérabilités de sécurité [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata "Nouvelle version amont">
<correction uglifyjs "Correction du contenu de la page de manuel">
<correction uriparser "Correction de plusieurs vulnérabilités de sécurité [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm "Suppression de la prise en charge de xemacs21">
<correction vulture "Ajout de la dépendance manquante à python3-pkg-resources">
<correction wayland "Correction d'un possible dépassement d'entier [CVE-2017-16612]">
<correction wicd "Dépendance systématique à net-tools plutôt qu'aux alternatives">
<correction wvstreams "Contournement de corruption de pile">
<correction xapian-core "Correction de fuites de blocs de la liste de blocs dans des cas particuliers, qui sont ensuite signalées comme <q>DatabaseCorruptError</q> par Database::check()">
<correction xkeycaps "Erreur de segmentation évitée dans commands.c quand plus de 8 symboles par touche sont présents">
<correction yosys "Correction de <q>ModuleNotFoundError : No module named 'smtio'</q>">
<correction z3 "Retrait de l'étiquette « Multi-Arch : same » incorrecte de python-z3">
</table>

<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction adblock-plus "Incompatible avec les dernières versions de firefox-esr">
<correction calendar-exchange-provider "Incompatible avec les dernières versions de Thunderbird">
<correction cookie-monster "Incompatible avec les dernières versions de firefox-esr">
<correction corebird "Cassé par les modifications de l'API de Twitter API">
<correction debian-buttons "Incompatible avec les dernières versions de firefox-esr">
<correction debian-parl "Dépend de greffons de Firefox cassés ou supprimés">
<correction firefox-branding-iceweasel "Incompatible avec les dernières versions de firefox-esr">
<correction firefox-kwallet5 "Incompatible avec les dernières versions de firefox-esr">
<correction flashblock "Incompatible avec les dernières versions de firefox-esr">
<correction flickrbackup "Incompatible avec l'API courante de Flickr">
<correction imap-acl-extension "Incompatible avec les dernières versions de firefox-esr">
<correction libwww-topica-perl "Inutile du fait de la fermeture du site Topica">
<correction mozilla-dom-inspector "Incompatible avec les dernières versions de firefox-esr">
<correction mozilla-noscript "Incompatible avec les dernières versions de firefox-esr">
<correction mozilla-password-editor "Incompatible avec les dernières versions de firefox-esr">
<correction mozvoikko "Incompatible avec les dernières versions de firefox-esr">
<correction personaplus "Incompatible avec les dernières versions de firefox-esr">
<correction python-formalchemy "Inutilisable, échec de l'importation dans Python">
<correction refcontrol "Incompatible avec les dernières versions de firefox-esr">
<correction requestpolicy "Incompatible avec les dernières versions de firefox-esr">
<correction spice-xpi "Incompatible avec les dernières versions de firefox-esr">
<correction toggle-proxy "Incompatible avec les dernières versions de firefox-esr">
<correction y-u-no-validate "Incompatible avec les dernières versions de firefox-esr">

</table>

<h2>Installateur Debian</h2>
<p>L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Adresse de l'actuelle distribution stable :</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>
