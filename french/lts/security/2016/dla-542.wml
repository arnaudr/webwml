#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>De nombreux problèmes de sécurité ont été identifiés et corrigés dans
Pidgin dans Debian <q>Wheezy</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2365">CVE-2016-2365</a>

<p>Vulnérabilité de déni de service dans la commande Markup de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2366">CVE-2016-2366</a>

<p>Vulnérabilité de déni de service dans la commande Table de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2367">CVE-2016-2367</a>

<p>Vulnérabilité de divulgation de mémoire de longueur d'Avatar de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2368">CVE-2016-2368</a>

<p>Plusieurs vulnérabilités de dépassement de tampon dans g_snprintf de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2369">CVE-2016-2369</a>

<p>Vulnérabilité de déni de service dans CP_SOCK_REC_TERM de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2370">CVE-2016-2370</a>

<p>Vulnérabilité de déni de service dans Custom Resource de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2371">CVE-2016-2371</a>

<p>Vulnérabilité d'exécution de code dans Extended Profiles de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2372">CVE-2016-2372</a>

<p>Vulnérabilité de divulgation de mémoire de longueur dans File Transfer de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2373">CVE-2016-2373</a>

<p>Vulnérabilité de déni de service dans Contact Mood de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2374">CVE-2016-2374</a>

<p>Vulnérabilité d'exécution de code dans MultiMX Message de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2375">CVE-2016-2375</a>

<p>Vulnérabilité de divulgation de mémoire de Suggested Contacts de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2376">CVE-2016-2376</a>

<p>Vulnérabilité d'exécution de code dans read stage 0x3 de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2377">CVE-2016-2377</a>

<p>Vulnérabilité de dépassement de tampon dans HTTP Content-Length de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2378">CVE-2016-2378</a>

<p>Vulnérabilité d'exécution de code dans get_utf8_string de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2380">CVE-2016-2380</a>

<p>Vulnérabilité de fuite d'information dans mxit_convert_markup_tx de MXIT</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4323">CVE-2016-4323</a>

<p>Vulnérabilité d'écrasement de fichier arbitraire de Splash Image de MXIT</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.10.10-1~deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pidgin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-542.data"
# $Id: $
