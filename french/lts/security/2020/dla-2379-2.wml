#use wml::debian::translation-check translation="aec5cd6bbf5a87ccacb02cc6f72a26b1c6f615e4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de mediawiki, publiée dans la DLA-2379-1, contenait un défaut
dans le correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25827">CVE-2020-25827</a>
qui faisait échouer le rendu de toutes les pages et renvoyait une erreur de
syntaxe. De nouveaux paquets mis à jour de mediawiki sont désormais disponibles
pour corriger ce problème.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1:1.27.7-1~deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2379-2.data"
# $Id: $
