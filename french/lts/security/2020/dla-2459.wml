#use wml::debian::translation-check translation="1f2c4cb5c3f79b8b9d200abc9c2320dec972bff7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans golang-1.7, un compilateur du langage
de programmation Go.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15586">CVE-2020-15586</a>

<p>Utiliser le <q>100-continue</q> dans les en-têtes HTTP reçus par un
serveur HTTP peut conduire à une compétition des données impliquant
l’écriture de la connexion mise en tampon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16845">CVE-2020-16845</a>

<p>Certaines entrées non valables pour ReadUvarint ou ReadVarint pourraient
permettre à ces fonctions de lire un nombre illimité d’octets à partir de
l’argument ByteReader avant de renvoyer une erreur.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.7.4-2+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-1.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-1.7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-1.7">https://security-tracker.debian.org/tracker/golang-1.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2459.data"
# $Id: $
