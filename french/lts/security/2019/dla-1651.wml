#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes dans libgd2, a bibliothèque graphique qui permet de
dessiner rapidement des images, ont été découverts.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6977">CVE-2019-6977</a>

<p>Une double libération potentielle de zone de mémoire dans gdImage*Ptr() a été
signalée par Solmaz Salimi (Rooney).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6978">CVE-2019-6978</a>

<p>Simon Scannell a trouvé un dépassement de tas, exploitable avec des données
contrefaites d’image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000222">CVE-2018-1000222</a>

<p>Une nouvelle vulnérabilité de double libération dans gdImageBmpPtr() a été
signalée par Solmaz Salimi (Rooney).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5711">CVE-2018-5711</a>

<p>Due à une erreur d’absence de signe d’entier, la fonction d’analyse du cœur de
GIF peut entrer dans une boucle infinie. Cela conduit à un déni de service et
à l’épuisement des ressources du serveur.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 2.1.0-5+deb8u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgd2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1651.data"
# $Id: $
