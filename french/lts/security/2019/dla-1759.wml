#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Des conditions de lecture et écriture hors limites ont été corrigées dans
 clamav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1787">CVE-2019-1787</a>

<p>Une condition de lecture de tas hors limites peut se produire lors de
l’analyse de documents PDF. Le défaut est une incapacité à correctement
garder trace du nombre d’octets restant dans un tampon lors de l’indexation
de données de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1788">CVE-2019-1788</a>

<p>Une condition d’écriture de tas hors limites peut se produire lors de
l’analyse de fichiers OLE2 tels ceux de documents Microsoft Office 97-2003.
L’écriture non valable se produit lorsqu’un pointeur non valable est par erreur
utilisé pour initialiser un entier de 32 bits à zéro. Cela conduira probablement
à un plantage de l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1789">CVE-2019-1789</a>

<p>Une condition de lecture de tas hors limites peut se produire lors de
l’analyse de fichiers PE (c'est-à-dire, fichiers EXE et DLL de Windows) qui ont
été empaquetés avec Aspack, à la suite d’une vérification inadéquate de limite.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.100.3+dfsg-0+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets clamav.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1759.data"
# $Id: $
