#use wml::debian::translation-check translation="4b462ea192fc355c557625a4edd8b02668ca91dd" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que freeimage, une bibliothèque graphique, était
affectée par les deux problèmes de sécurité suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

<p>Un dépassement de tas causé par un appel memcpy non valable dans
PluginTIFF. Ce défaut pourrait être exploité par des attaquants distants
pour déclencher un déni de service ou tout autre impact non précisé
à l'aide de données TIFF contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

<p>Épuisement de pile causé par une récursion non désirée dans PluginTIFF.
Ce défaut pourrait être exploité par des attaquants distants pour
déclencher un déni de service à l'aide de données TIFF contrefaites.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 3.17.0+ds1-5+deb9u1.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 3.18.0+ds2-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freeimage.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de freeimage, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"
# $Id: $
