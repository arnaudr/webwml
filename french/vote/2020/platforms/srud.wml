#use wml::debian::template title="Programme de Sruthi Chandran" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="02a6fb7c7c7b6dae428f0807fde2fc83ada31179" maintainer="Jean-Pierre Giraud"

<h2>Qui suis-je ?</h2>
<p>J'ai trente-deux ans et je suis Indienne. Je suis une membre relativement
récente de Debian (responsable de paquet depuis 2016, développeuse Debian
depuis 2019). À la différence de la plupart des développeurs Debian, je n'ai
pas une longue histoire d'association à Debian. J'étais bibliothécaire sans
aucune expérience technique, travaillant dans une bibliothèque d'entreprise,
avant d'être initiée au logiciel libre et à Debian en 2015/2016. À partir de
là, tout a changé. Je suis devenue une défenseure du logiciel libre et
contributrice à Debian et, quelques années plus tard, je suis devenue une
développeuse Debian.
</p>
<p>Je fais partie des équipes Ruby, JavaScript et Golang de Debian.
J'entretiens bon nombre de paquets dont les principaux sont gitlab, gitaly,
rails, etc.
</p>
<p>J'ai parrainé plusieurs contributeurs de Debian. Je suis impliquée dans de
nombreux ateliers d'empaquetage et d'autres événements de Debian à travers
l'Inde.
</p>
<p>Je pilote l'équipe de candidature de l'Inde pour la DebConf qui a obtenu la
tenue de DebConf22.</p>
<p><br/>

</p>
<h2>Pourquoi je concours ?</h2>
<p>Préoccupée par le ratio hommes-femmes déséquilibré dans la communauté du
logiciel libre (et dans Debian), je m'efforce de faire toutes les petites
choses que je peux pour améliorer la situation. Cette candidature de
responsable du projet Debian est aussi, en fait, un petit pas dans cette
direction. Combien de fois avons-nous eu une candidature non masculine au
poste de responsable du projet ? L'objectif principal de ma candidature est de
placer les problèmes de diversité au centre des préoccupations.</p>
<p>Je suis consciente que Debian fait des choses pour améliorer la diversité
en son sein, mais comme on peut le constater, cela n'est pas suffisant. Une
somme considérable d'argent est dépensée, mais cela nous amène-t-il des
contributeurs divers et de qualité ? Nous devons trouver des réponses. Nous
devons découvrir des moyens meilleurs et plus efficaces.</p>
<p>Une des manières efficaces que je vois pour encourager des personnes
différentes à contribuer est d'accorder plus de visibilité à la diversité qui
existe au sein de la communauté. Je voudrais encourager plus de femmes (aussi
bien cisgenres que transgenres), les hommes trans et les personnes non-binaires
qui font déjà partie du projet à se rendre plus visibles plutôt que de se
cacher quelque part dans le projet (comme je l'ai fait jusqu'à récemment).</p>
<p><br/>

</p>
<h2>Mon projet comme responsable du projet Debian</h2>
<p>Je n'ai pas de projet extraordinaire comme responsable du projet Debian.
Mais voici quelques choses simples sur lesquelles j'aimerais traiter au-delà
des activités habituelles de responsable du projet Debian. Je ne suis pas bonne
pour rédiger un projet détaillé, mais suis ouverte aux discussions sur chacun
des points suivants durant la campagne si nécessaire :</p>
<ul>
	<li><p>être une responsable du projet accessible, ouverte aux suggestions et à la discussion ;</p>
	<li><p>piloter des actions ciblant l'amélioration de la diversité dans Debian ;</p>
	<li><p>apporter une attention particulière à un meilleur emploi du budget consacré à la diversité ;</p>
	<li><p>rendre Debian plus accueillante pour tous – débutants autant qu'anciens ;</p>
	<li><p>encourager et faciliter la proposition de nouvelles idées et les processus pour améliorer Debian ;</p>
	<li><p>des discussions et activités pour des changements positifs et surmonter la réticence aux changements visibles aux différents niveaux du projet ;</p>
	<li><p>une meilleure délégation des responsabilités ;</p>
	<li><p>favoriser les discussions sur les moyens pour améliorer la &quot;do-o-cracy&quot; dans le projet ;</p>
	<li><p>promouvoir les réunions et les rencontres d'équipe en face à face ;</p>
	<li><p>encourager la réduction des discussions outrancières et mettre l'accent sur une meilleure communication entre équipes et individus.</p>
</ul>
<p>
<br/>
<br/>

</p>
<h2>Engagement en temps</h2>
<p>En tant que travailleuse indépendante, avec comme travail quotidien
l’empaquetage pour Debian, cela ne me sera absolument pas un problème
d'accorder du temps aux tâches de responsable du projet.</p>

