<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that psensor, a server for monitoring hardware
sensors remotely, was prone to a directory traversal vulnerability
because the create_response function in server/server.c lacks a check
for whether a file is under the webserver directory.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.6.2.17-2+deb7u1.</p>

<p>We recommend that you upgrade your psensor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1361.data"
# $Id: $
