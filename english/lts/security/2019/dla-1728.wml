<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple scp client vulnerabilities have been discovered in OpenSSH, the
premier connectivity tool for secure remote shell login and secure file
transfer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

    <p>In scp.c, the scp client allowed remote SSH servers to bypass
    intended access restrictions via the filename of . or an empty
    filename. The impact was modifying the permissions of the target
    directory on the client side.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

    <p>Due to missing character encoding in the progress display, a
    malicious server (or Man-in-The-Middle attacker) was able to employ
    crafted object names to manipulate the client output, e.g., by using
    ANSI control codes to hide additional files being transferred. This
    affected refresh_progress_meter() in progressmeter.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

    <p>Due to the scp implementation being derived from 1983 rcp, the server
    chooses which files/directories are sent to the client. However, the
    scp client only performed cursory validation of the object name
    returned (only directory traversal attacks are prevented). A
    malicious scp server (or Man-in-The-Middle attacker) was able to
    overwrite arbitrary files in the scp client target directory. If
    recursive operation (-r) was performed, the server was able to
    manipulate subdirectories, as well (for example, to overwrite the
    .ssh/authorized_keys file).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:6.7p1-5+deb8u8.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1728.data"
# $Id: $
