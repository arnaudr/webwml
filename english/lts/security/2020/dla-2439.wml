<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In libexif/exif-entry.c, through libexif 0.6.21-2+deb9u4,
compiler optimization could remove a buffer overflow check,
making a buffer overflow possible with some EXIF tags.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.6.21-2+deb9u5.</p>

<p>We recommend that you upgrade your libexif packages.</p>

<p>For the detailed security status of libexif please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libexif">https://security-tracker.debian.org/tracker/libexif</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2439.data"
# $Id: $
