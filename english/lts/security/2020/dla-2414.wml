<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In fastd, a fast and secure tunnelling daemon, a receive buffer
handling problem was discovered which allows a denial of service
(memory exhaustion) when receiving packets with an invalid type code.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
18-2+deb9u1.</p>

<p>We recommend that you upgrade your fastd packages.</p>

<p>For the detailed security status of fastd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/fastd">https://security-tracker.debian.org/tracker/fastd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2414.data"
# $Id: $
