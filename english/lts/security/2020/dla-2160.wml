<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two security issues have been identified and fixed in php5, a
server-side, HTML-embedded scripting language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7062">CVE-2020-7062</a>

<p>is about a possible null pointer derefernce, which would
likely lead to a crash, during a failed upload with progress tracking.
<a href="https://security-tracker.debian.org/tracker/CVE-2020-7063">CVE-2020-7063</a> is about wrong file permissions of files added to tar with
Phar::buildFromIterator when extracting them again.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.40+dfsg-0+deb8u10.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2160.data"
# $Id: $
