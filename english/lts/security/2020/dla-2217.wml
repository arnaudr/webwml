<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential remote code execution via
deserialization in tomcat7, a server for HTTP and Java "servlets".</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

    <p>When using Apache Tomcat versions 10.0.0-M1 to 10.0.0-M4, 9.0.0.M1 to
    9.0.34, 8.5.0 to 8.5.54 and 7.0.0 to 7.0.103 if a) an attacker is able to
    control the contents and name of a file on the server; and b) the server is
    configured to use the PersistenceManager with a FileStore; and c) the
    PersistenceManager is configured with
    sessionAttributeValueClassNameFilter="null" (the default unless a
    SecurityManager is used) or a sufficiently lax filter to allow the attacker
    provided object to be deserialized; and d) the attacker knows the relative
    file path from the storage location used by FileStore to the file the
    attacker has control over; then, using a specifically crafted request, the
    attacker will be able to trigger remote code execution via deserialization
    of the file under their control. Note that all of conditions a) to d) must
    be true for the attack to succeed.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
7.0.56-3+really7.0.100-1+deb8u1.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2217.data"
# $Id: $
