<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a buffer overflow vulnerability in
<a href="http://slirp.sourceforge.net/">slirp</a>, a SLIP/PPP emulator for
using a dial up shell account. This was caused by the incorrect usage of return
values from <tt>snprintf(3)</tt>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

    <p>In libslirp 4.1.0, as used in QEMU 4.2.0, tcp_subr.c misuses snprintf
    return values, leading to a buffer overflow in later code.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:1.0.17-7+deb8u2.</p>

<p>We recommend that you upgrade your slirp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2142.data"
# $Id: $
