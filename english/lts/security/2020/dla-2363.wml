<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>asyncpg before 0.21.0 allows a malicious PostgreSQL server to trigger
a crash or execute arbitrary code (on a database client) via a crafted
server response, because of access to an uninitialized pointer in the
array data decoder.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.8.4-1+deb9u1.</p>

<p>We recommend that you upgrade your asyncpg packages.</p>

<p>For the detailed security status of asyncpg please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/asyncpg">https://security-tracker.debian.org/tracker/asyncpg</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2363.data"
# $Id: $
