#use wml::debian::translation-check translation="122cd4d0371114e8cb5f8eb2f4d7b3d228204e1e" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8625">CVE-2019-8625</a>

    <p>Sergei Glazunov opdagede at ondsindet fremstillet webindhold kunne føre 
    til universel udførelse af skripter på tværs af servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8720">CVE-2019-8720</a>

    <p>Wen Xu opdagede at ondsindet fremstillet webindhold kunne føre til 
    udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8769">CVE-2019-8769</a>

    <p>Pierre Reimertz opdagede at besøg på et ondsindet fremstillet websted 
    kunne afsløre browserhistorik.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8771">CVE-2019-8771</a>

    <p>Eliya Stein opdagede at ondsindet fremstillet webindhold kunne overtræde 
    iframe-sandkasse-regler.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.26.1-3~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4558.data"
