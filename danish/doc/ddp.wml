#use wml::debian::ddp title="Debians dokumentationsprojekt"
#use wml::debian::translation-check translation="60eea6916091f1a3a9240f472ebea1904c32d0bd"

<p>Debians dokumentationsprojekt (DDP) blev dannet for at kunne koordinere og 
forene alle tiltag til at skrive mere og bedre dokumentation om 
Debian-systemet.</p>

<h2>DDP's arbejde</h2>
<div class="line">
  <div class="item col50">

<h3>Håndbøger</h3>
<ul>
  <li><strong><a href="user-manuals">Brugerhåndbøger</a></strong></li>
  <li><strong><a href="devel-manuals">Udviklerhåndbøger</a></strong></li>
  <li><strong><a href="misc-manuals">Forskellige håndbøger</a></strong></li>
  <li><strong><a href="#other">Problematiske håndbøger</a></strong></li>
</ul>
</div>

  <div class="item col50 lastcol">

<h3>Fremgangsmåde for dokumentation</h3>
<ul>
  <li>Vejledningernes licenser følger DFSG</li>
  <li>We use Docbook XML for our documents</li>
  <li>Kildekoden bør være tilgængelig fra <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></li>
  <li><tt>www.debian.org/doc/&lt;manual-name&gt;</tt> er den officielle URL</li>
  <li>Alle dokumenter skal vedligeholdes aktivt</li>
  <li>Stil spørgsmål på engelsk på <a href="https://lists.debian.org/debian-doc/">debian-doc</a>, hvis du har lyst til at skrive et nyt dokument</li>
</ul>

<h3>Git-adgang</h3>
<ul>
  <li><a href="vcs">Hvordan man tilgår</a> DDP's git-arkiver</li>
</ul>

</div>

</div>

<hr>

<h2><a name="other">Problematiske håndbøger</a></h2>

<p>Ud over de ovennævnte håndbøger, vedligeholder vi følgende håndbøger, som er 
problematiske på forskellig vis, hvorfor vi ikke kan anbefale dem til alle 
brugere. Caveat emptor (køber, pas på).</p>

<ul>
  <li><a href="obsolete#tutorial">Debian-lærebog</a>, forældet</li>
  <li><a href="obsolete#guide">Debian Guide</a>, forældet</li>
  <li><a href="obsolete#userref">Debians brugeropslagsbog</a>, gået i stå
      og ganske ufuldstændig</li>
  <li><a href="obsolete#system">Debians system-administrationshåndbog</a>,
      gået i stå, næsten uden indhold</li>
  <li><a href="obsolete#network">Debians netværksadministrationshåndbog</a>,
      gået i stå, ufuldstændig</li>
  <li><a href="obsolete#swprod">Hvordan software-producenter kan 
      distribuere deres produkter direkte i .deb-formatet</a>, gået i stå, forældet</li>
  <li><a href="obsolete#packman">Debians pakningshåndbog</a>, delvist 
      benyttet i <a href="devel-manuals#policy">Debians retningsliniehåndbog</a>
      resten vil blive anvendt i en dpkg-opslagsbog som ikke er skrevet
      endnu.</li>
  <li><a href="obsolete#makeadeb">Introduktion: Fremstil en Debian-pakke</a>
      forældet af
      <a href="devel-manuals#maint-guide">Debians håndbog til nye vedligeholdere</a>
      og
      <a href="devel-manuals#debmake-doc">Håndbog til Debian-vedligeholdere</a></li>
  <li><a href="obsolete#programmers">Debians håndbog til programmører</a> 
      forældet af
      <a href="devel-manuals#maint-guide">Debians håndbog til nye vedligeholdere</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, forældet efter 
      introduktionen af secure APT</li>
  <li><a href="obsolete#i18n">Introduktion til i18n</a>, gået i stå</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML HOWTO</a>, gået i stå, forældet</li>
  <li><a href="obsolete#markup">Debiandoc-SGML Markup Manual</a>, gået i stå; DebianDoc er ved at blive fjernet</li>
</ul>
