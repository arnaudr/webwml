#use wml::debian::template title="Το Debian στην Επιφάνεια Εργασίας"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="fa8e80162815f92b0d792ca4837df20cdc61c896" maintainer="galaxico"

<h2>Το Οικουμενικό Λειτουργικό Σύστημα για την Επιφάνεια Εργασίας σας</h2>

<p>
  Το υπο-έργο Debian Desktop είναι μια ομάδα εθελοντών που θέλουν να δημιουργήσουν το καλλίτερο δυνατό λειτουργικό σύστημα για χρήση τόσο στο σπίτι όσο και για επαγγελματικούς σταθμούς εργασίας. Το σλόγκαν μας είναι  <q>Software that Just Works</q>. Με λίγα λόγια, ο σκοπός μας είναι να φέρουμε το Debian, οτ GNU, και το Linux στην επικρατούσα πραγματικότητα.
  <img style="float: right;" src="debian-desktop.png" alt="Debian Desktop"/>
</p>
<h3>Οι αρχές μας</h3>
<ul>
  <li>
    Αναγνωρίζοντας ότι υπάρχουν πολλά <a href="https://wiki.debian.org/DesktopEnvironment">Περιβάλλοντα επιφάνειας εργασίας</a>, θα υποστηρίξουμε την χρήση τους και θα διασφαλίσουμε ότι δουλεύουν καλά με το Debian.
  </li>
  <li>
    Αναγνωρίζουμε ότι υπάρχουν μόνο δύο σημαντικές ομάδες χρηστών: οι αρχάριοι και οι ειδικοί. Θα κάνουμε ό,τι μπορούμε για να είναι τα πράγματα εύκολο για τον αρχάριο επιτρέποντας, ταυτόχρονα, στον ειδικό να "πειράξει" τα πράγματα αν το επιθυμεί
  </li>
  <li>
    Θα προσπαθήσουμε να διασφαλίσουμε ότι το λογισμικό ρυθμίζεται για την πιο κοινή χρήση της επιφάνειας εργασίας. Για παράδειγμα, ο λογαριασμός του συνηθισμένου χρήστη που προστίθεται εξ ορισμού στη διάρκεια της εγκατάστασης θα πρέπει να έχει τα δικαιώματα ώστε να παίξει ήχο και βίντεο και να διαχειριστεί το σύστημα μέσω του μηχανισμού sudo.
  </li>
  <li>
    <p>
    Θα προσπαθήσουμε να διασφαλίσουμε ότι ερωτήσεις που γίνονται στον χρήστη
    (και οι οποίες θα πρέπει να κρατηθούν σε ένα ελάχιστο) βγάζουν νόημα ακόμα και με ένα ελάχιστο γνώσης υπολογιστών. Μερικά πακέτα του Debian σήμερα παρουσιάζουν για τον χρήστη δύσκολες τεχνικές λεπτομέρειες. Τεχνικές ερωτήσεις από το debconf που τίθενται στον χρήστη από τον εγκαταστάτη του  Debian θα πρέπει να αποφεύγονται. Για τον αρχάριο χρήστη, τέτοιες ερωτήσεις προκαλού συχνά σύγχυση και φόβο. Για τον ειδικό, ίσως είναι ενοχλητικές και άχρηστες. Ένας αρχάριος ίσως να μην ξέρει καν τι αφορούν αυτές οι ερωτήσεις. Ένας ειδικός μπορεί να ρυθμίσει το περιβάλλον εργασίας όπως του/της αρέσει μετά την ολοκλήρωση της εγκατάστασης. Η προτεραιότητα αυτού του είδους ερωτήσεων του Debconf θα πρέπει τουλάχιστον να μειωθεί.
    </p>
  <li>
    Και κάνοντας όλα αυτά θα διασκεδάζουμε!
  </li>
</ul>
<h3>Πώς μπορείτε να βοηθήσετε</h3>
<p>
  Τα πιο σημαντικά μέρη ενός υπο-έργου του Debian δεν είναι οι λίστες αλληλογραφίας, οι ιστοσελίδες ή ο χώρος στις αρχειοθήκες για πακέτα. Το πιο σημαντικό μέρος είναι <em>άτομα με κίνητρο</em> που κάνουν τα πράγματα να συμβούν. Δεν χρειάζεται να είστε ένας επίσημος προγραμματιστής/τρια για να ξεκινήσετε να κάνετε πακέτα και διορθώσεις. Η βασική ομάδα του Debian Desktop θα εξασφαλίσει ότι η δουλειά σας θα ενσωματωθεί. Οπότε, αυτά είναι μερικά από τα πράγματα που μπορείτε να κάνετε:
</p>
<ul>
  <li>
    Δοκιμάστε το <q>Desktop Default Environment</q> task (ή το kde-desktop task), 
    εγκαθιστώντας μια από  <a href="$(DEVEL)/debian-installer/">δοκιμαστικές εικόνες της επόμενης έκδοσης</a> και στείλτε το feedback στην λίστα αλληλογραφίας <a 
    href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
  </li>
  <li>
    Δουλειά πάνω στον <a href="$(DEVEL)/debian-installer/">debian-installer</a>.  
    Το GTK+ frontend σας χρειάζεται.
  </li>
  <li>
Βοηθήστε την <a href="https://wiki.debian.org/Teams/DebianGnome">ομάδα GNOME του Debian</a>, την
<a href="https://qt-kde-team.pages.debian.net/">Ομάδα Qt και KDE του Debian</a> ή την
<a href="https://salsa.debian.org/xfce-team/">Ομάδα Xfce του Debian</a>.
Μπορείτε να βοηθήσετε με τη δημιουργία πακέτων, τη διαλογή σφαλμάτων, την τεκμηρίωση, με δοκιμές και πολλά άλλα.
  </li>
  <li>
    Διδάξτε στους χρήστες πώς να εγκαθιστούν και να χρησιμοποιούν τα "καθήκοντα" (task) της Επιφάνειας Εργασίας του Debian που διαθέτουμε τώρα
    (desktop, gnome-desktop και kde-desktop).
  </li>
  <li>
    Δουλέψτε για τη ελάττωση της προτεραιότητας ή την αφαίρεση μη αναγκαίων προτρεπτικών ερωτήσεων του
    <a href="https://packages.debian.org/debconf">debconf</a> από πακέτα και για να γίνουν αυτές που είναι απαραίτητες εύκολο να κατανοηθούν.
  </li>
  <li>
    Βοηθήστε στην προσπάθεια <a href="https://wiki.debian.org/DebianDesktop/Artwork">Debian 
    Desktop Artwork</a>.
  </li>
</ul>
<h3>Wiki</h3>
<p>
  Έχουμε μερικά άρθρα στο wiki μας, και το σημεία αφετηρίς μας είναι: 
  <a href="https://wiki.debian.org/DebianDesktop">DebianDesktop</a>. Μερικά άρθρα στο wiki του Debian Desktop είναι παρωχημένα.
</p>
<h3>Λίστα αλληλογραφίας</h3>
<p>
  Το παρόν υπο-έργο συζητείται στην λίστα αλληλογραφίας
  <a href="https://lists.debian.org/debian-desktop/">debian-desktop</a>.
</p>
<h3>Κανάλι IRC</h3>
<p>
  Ενθαρρύνουμε καθέναν/καθεμία (είτε είναι προγραμματιστής/τρια του Debian είτε όχι) που ενδιαφέρεται για το Debian στην Επιφάνεια Εργασίας να παρακολουθεί το κανάλι #debian-desktop στο <a href="http://oftc.net/">OFTC IRC</a> (irc.debian.org).
</p>
<h3>Ποιοι/ες εμπλέκονται;?</h3>
<p>
  Οποιοσδήποτε/οποιαδήποτε θέλει να εμπλακεί είναι ευπρόσδεκτος/η. Στην πραγματικότητα, όλοι/ες στις ομάδες pkg-gnome, pkg-kde και pkg-xfce εμπλέκονται έμμεσα. Οι εγγεγραμμένοι στη λίστα αλληλογραφίας debian-desktop συνεισφέρουν ενεργά. Οι ομάδες του debian installer και του tasksel είναι επίσης σημαντικές για τους στόχους μας.
</p>

<p>
  Αυτή η ιστοσελίδα συντηρείται από τον <a href="https://people.debian.org/~stratus/">\
  Gustavo Franco</a>. Προηγούμενοι συντηρητές είναι οι
  <a href="https://people.debian.org/~madkiss/">Martin Loschwitz</a> και
  <a href="https://people.debian.org/~walters/">Colin Walters</a>.
</p>
