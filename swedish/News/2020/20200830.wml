#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20"
<define-tag pagetitle>DebConf20 online avslutas</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
Lördagen den 29 Augusti 2020, avslutades den årliga Debiankonferensen för
utvecklare och bidragslämnare.
</p>

<p>
DebConf20 har hållits online för första gången, på grund av coronaviruspandemin
(COVID-19).
</p>

<p>
Alla sessioner har strömmats, med ett antal sätt att deltaga:
via IRC-meddelanden, textdokument via onlinesamarbete, och
videokonferansmötesrum.
</p>

<p>
Med mer än 850 deltagare från 80 olika länder och totalt mer än
100 evenemangstal, diskussionssessioner, Birds of a Feather-sessioner (BoF)
och andra aktiviteter, var <a href="https://debconf20.debconf.org">DebConf20</a>
en stor framgång.
</p>

<p>
När det blev klart att DebConf20 skulle vara ett online-evenemang endast,
spenderade DebConf videogrupp en stor del tid under de följande månaderna för
att adaptera, förbättra, och i vissa fall skapa ny teknologi från grunden,
som skulle krävas för att göra en DebConf online möjlig. Efter lärdomar
från MiniDebConfOnline i slutet av maj gjordes några justeringar, och
slutligen kom vi fram till en uppsättning som inkluderar Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad och en nyskriven webbaserad
frontend för voctomix som de olika delarna.
</p>

<p>
Alla komponenter i videoinfrastrukturen är fri mjukvara, och hela
uppsättningen konfigureras via deras publika
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>-förråd.
</p>

<p>
<a href="https://debconf20.debconf.org/schedule/">Schemat</a> för DebConf20
inkluderade två spår i andra språk än engelska: spanskspråkiga MiniConf,
med åtta föreläsningar under två dagar, och MiniConf för malayalamtalande, med
nio föreläsningar under tre dagar.
Ad-hoc-aktiviteter, som införs av deltagare under hela konferensen har också
varit möjliga, strömmade och inspelade. Det har även varit flera gruppmöten
för att <a href="https://wiki.debian.org/Sprints/">sprinta</a> vissa områden
inom Debianutveckling.
</p>

<p>
Mellan föreläsningar har videoströmmen visat de vanliga sponsorerna i loopen,
men även ytterligare videos som foton från tidigare DebConfs, intressanta fakta
om Debian och korta shout-out-videos som skickats in av deltagare för att
kommunicera med deras Debianvänner.
</p>

<p>
För dom som inte hade möjligheten att delta, finns de flesta av föreläsningarna
och sessionerna redan tillgängliga genom 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">webbplatsen
för arkivet över Debianmöten</a>,
och de återstående kommer att läggas till under dom kommande dagarna.
</p>

<p>
Webbplatsen för <a href="https://debconf20.debconf.org/">DebConf20</a> kommer
att hållas aktiv för arkiveringsändamål och kommer att fortsätta att erbjuda
länkar till presentationerna och videos av föreläsningar och evenemang.
</p>

<p>
Nästa år planeras <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> att
hållas i Haifa, Israel, i augusti eller september.
</p>

<p>
DebConf är hängivet till en säker och välkomnande miljö för alla deltagare.
Under konferensen har flera grupper (Front Desk, Välkomstgruppen och
Gemnskapsgruppen) funnits tillgängliga så att deltagare får den bästa
upplevelsen möjligt, och för att hitta lösningar på alla eventuella
problem som kan uppkomma.
Se <a href="https://debconf20.debconf.org/about/coc/">webbsidan om förhållningsregler på webbplatsen för DebConf20</a>
för ytterligare detaljer om detta.
</p>

<p>
Debian tackar för åtagandet från flera <a href="https://debconf20.debconf.org/sponsors/">sponsorer</a>
för att dom stödjer DebConf20, särskilt våra Platina-sponsorer:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
och 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>Om Debian</h2>
<p>
	Debianprojektet grundades 1993 av Ian Murdock med målsättningen att vara ett
	i sanning fritt gemenskapsprojekt. Sedan dess har projektet vuxit till att
	vara ett av världens största och mest inflytelserika öppenkällkodsprojekt.
	Tusentals frivilliga från hela världen jobbar tillsammans för att skapa
	och underhålla Debianmjukvara. Tillgängligt i 70 språk, och med stöd för
	en stor mängd datortyper, kallar sig Debian det <q>universella 
	operativsystemet</q>.
</p>

<h2>Om DebConf</h2>

<p>
DebConf är Debianprojektets utvecklarkonferens. Utöver ett fullt schema med
tekniska, sociala och policyföreläsningar, ger DebConf en möjlighet för
utvecklare, bidragslämnare och andra intresserade att mötas personligen
och jobba närmare tillsammans. Det har ägt rum årligen sedan 2000 på
så vitt skilda platser som Scotland, Argentina, Bosnien och Hercegovina.
Mer information om DebConf finns tillgänglig på 
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Om Lenovo</h2>

<p>
Som en global teknologiledare som tillverkar en bredd av anslutna produkter,
inklusive mobiltelefoner, plattor, PCs och arbetsstationer så väl som
AR/VR-enheter, smarta hem-/kontors-, och datacenterlösningar, förstår
<a href="https://www.lenovo.com">Lenovo</a> hur kritiskt det är med öppna
system och plattformar för en ansluten värld.
</p>

<h2>Om Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> är Schweiz störa webbhostingföretag,
som även erbjuder backup- och lagringstjänster, lösningar för organisatörer av evenemang,
live-streaming och video on demand-tjänster.
Dom äger sina datacenter själva och alla element som är kritiska för
tjänsternas funktionalitet och produkter som tillhandahålls av företaget
(både mjukvara och hårdvara). 
</p>

<h2>Om Google</h2>

<p>
<a href="https://google.com/">Google</a> är ett av världens största teknologiföretag,
som tillhandahåller en bredd av Internet-relaterade tjänster och produkter så
som online-reklamteknologier, sökning, molnplatformar, mjukvara, och hårdvara.
</p>

<p>
Google har stött Debian genom att sponsra DebConf under mer än tio år,
och är även en Debianpartner som sponsrar delar av 
<a href="https://salsa.debian.org">Salsa</a>'s continuous integration-infrastruktur
inom Google Cloud-plattformen.
</p>

<h2>Om Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> är en av världens
mest omfattande och brett antagna molnplatformar,
som erbjuder mer än 175 fullt utrustade tjänster från datacenter globalt
(i 77 tillgänglighetszoner i 24 geografiska regioner).
Kunder till AWS inkluderar dom snabbast växande nystartade företagen, större
företag och ledande myndigheter.
</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare informationm vänligen besök DebConf20s webbplats på
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
eller skicka e-post till &lt;press@debian.org&gt;.</p>
