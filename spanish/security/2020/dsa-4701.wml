#use wml::debian::translation-check translation="90833ca5169a5ef4cdeac320dd3d7016a5d5f8d2"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Esta actualización incluye microcódigo de CPU actualizado para algunos tipos de CPU Intel y
proporciona mitigaciones para las vulnerabilidades hardware de
muestreo de datos de registros especiales («Special Register Buffer Data Sampling»)
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>),
muestreo de registros vectoriales («Vector Register Sampling»)
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0548">CVE-2020-0548</a>)
y muestreo de expulsión de L1D («L1D Eviction Sampling»)
(<a href="https://security-tracker.debian.org/tracker/CVE-2020-0549">CVE-2020-0549</a>).</p>

<p>La actualización de microcódigo para las CPU HEDT y Xeon con firma («signature») 0x50654, que
fue revertida en el DSA 4565-2, se incluye de nuevo, corregida.</p>

<p>La actualización del proyecto original para Skylake-U/Y (firma, o «signature», 0x406e3) tuvo que
excluirse de esta actualización tras recibirse informes de cuelgues en el arranque del sistema.</p>

<p>Para más detalles, consulte
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00320.html</a>,
<a href="https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html">\
https://www.intel.com/content/www/us/en/security-center/advisory/intel-sa-00329.html</a>.</p>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 3.20200609.2~deb9u1.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 3.20200609.2~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de intel-microcode.</p>

<p>Para información detallada sobre el estado de seguridad de intel-microcode, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4701.data"
