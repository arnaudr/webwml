#use wml::debian::template title="FAQ da política de patente de distribuição da comunidade" BARETITLE="true"
#use wml::debian::translation-check translation="ba01cfdc529712e3626bdf15fd37d39e94126794" maintainer="Thiago Pezzo (Tico)"

<pre><code>Versão: 1.0
Publicada: 8 de julho de 2011
</code></pre>

<h1>Introdução</h1>

<h2>Para quem este documento se dirige?</h2>

<p>Este documento apresenta informações úteis sobre patentes e responsabilidades
de patentes para desenvolvedores(as) trabalhando nas distribuições da comunidade
de Software Livre e Aberto (FOSS - Free and Open Source Software). Por
distribuições da comunidade queremos dizer as coleções de pacotes mantidos e
distribuídos por organizações compostas por voluntários(as), pelas quais
organizações e voluntários(as) não buscam lucrar ao exercer essa atividade. Tais
distribuições baseadas na comunidade podem vender como também doar o produto de
seus trabalhos, possivelmente em CDs ou mídias de armazenamento USB, ou por
downloads pagos, como também por distribuição gratuita.</p>

<p>Este documento foi preparado por advogados(as) do
<a href="http://www.softwarefreedom.org">Software Freedom Law Center</a> (SFLC)
a pedido do <a href="https://www.debian.org/">projeto Debian</a>, e pode ser
útil para comunidades similares de distribuições FOSS. Suas declarações sobre
matérias legais são precisas em relação à data de elaboração e à legislação dos
Estados Unidos, e podem ser aplicáveis em outros sistemas legais. Mas este
documento não constitui um aconselhamento jurídico. Ele não foi baseado em
análises de qualquer situação factual particular, e qualquer advogado(a) que
opine sobre as questões apresentadas abaixo precisaria confirmar os fatos e
circunstâncias particulares que poderiam fazer variar esta informação em uma
determinada situação. Você não deve se fundamentar nesse documento para tomar
decisões que afetem os direitos legais de seu projeto ou que afetem as
responsabilidades em situações reais sem consultar o SFLC ou outros(as)
advogados(as).</p>

<h1>Contexto sobre patentes</h1>

<h2>O que é uma patente?</h2>

<p>Uma patente é um monopólio outorgado pelo Estado que garante ao(à)
inventor(a) exclusividade nos direitos de fabricação, venda, oferecimento para
venda, oferecimento para fabricação ou importação da invenção reivindicada pelo
período limitado pela patente. O(A) detentor(a) da patente pode então licenciar,
em caráter exclusivo ou não exclusivo, um ou mais dos direitos garantidos.</p>

<h2>Qual o tempo da vigência da patente?</h2>

<p>Geralmente, na maioria dos governos as patentes concedidas nos últimos 15
anos expiram em 20 anos da data de preenchimento do pedido de patente. Patentes
dos Estados Unidos com data de requisição antes de 8 de junho de 1995 fornecem
proteção por até 17 anos contando a data de concessão, ou 20 anos da data de
requisição, o que ocorrer depois.</p>

<p>No Brasil, a patente de invenção tem vigência de 20 anos e a patente de
utilidade tem vigência de 15 anos contados da data de depósito.</p>

<p>Existem exceções. Patentes podem ter seu período de vigência estendidos por
determinação administrativa ou por um tribunal, mas isso raramente acontece com
patentes de software. O tempo de vigência também pode ser encurtado por acordo
com o requerente durante um <q>processo judicial</q>, isto é, durante o
procedimento administrativo de patente que resulta em uma concessão. Se o
período de vigência de uma patente foi encurtado durante um processo judicial,
um <q>aviso legal de terminalidade</q> aparecerá na primeira página da
patente.</p>

<h2>Qual a diferença entre a proteção de patente e a proteção de copyright?</h2>

<p>O(A) proprietário(a) do copyright tem o direito de evitar que outras pessoas
façam cópias não autorizadas do programa sob direitos autorais, mas não podem
evitar programas criados independentemente com as mesmas funcionalidades. A
criação independente é, portanto, uma defesa completa para alegações de
violação de copyright. Além disso, o <q>uso justo</q> é uma defesa contra
violação de copyright, ou uma limitação substantiva do copyright, em todos os
sistemas de copyright. A legislação de patente carece de qualquer dispensa
por uso justo, de modo que criação independente, uso para pesquisa ou
engenharia reversa para propósitos de interoperabilidade ou educacionais não são
defesas contra alegações de violação de patente.</p>

<h2>Existe uma patente mundial ou algo parecido?</h2>

<p>No momento, não existem patentes mundiais. Fora da União Europeia, onde
requisições podem ser consolidadas, as patentes geralmente devem ser
requisitadas em cada país no qual deseja-se a proteção de patente.</p>

<h2>O que são reivindicações de patente?</h2>

<p>Reivindicações, que são a parte mais importante da patente, determinam o
escopo real da invenção para o qual a patente se aplica. É somente a
reivindicação que define quais os direitos exclusivos cobertos, ou seja,
praticar o que foi reivindicado sem uma licença é uma violação. Ler e entender
as reivindicações de uma patente é a chave para determinar se um dado produto
ou processo a viola.</p>

<p>Cada reivindicação está em uma única sentença. Reivindicações começam com um
<q>preâmbulo</q> seguido de uma ou mais <q>limitações</q>.</p>

<p>Para que um software viole uma patente, o software (ou o sistema com o
software incorporado) deve implementar tudo o que está descrito em uma das
reivindicações da patente. Se você não pratica um ou mais dos elementos da
reivindicação, então você não viola diretamente essa reivindicação.</p>

<h2>O que são reivindicações independentes?</h2>

<p>Uma reivindicação de patente é chamada de <q>independente</q> se ela não faz
referências à nenhuma outra reivindicação de uma patente.</p>

<h2>O que são reivindicações dependentes?</h2>

<p>Reivindicações dependentes explicitamente incorporam o conteúdo de outras
reivindicações na patente. Uma reivindicação dependente é necessariamente mais
restrita em escopo do que a reivindicação da qual ela depende, porque isso
inclui uma ou mais limitações. Em termos do diagrama de Venn, a área de
cobertura de uma reivindicação dependente está totalmente contida dentro da
cobertura da reivindicação que ela referencia.</p>

<h2>Como são escritas as reivindicações de patentes relativas a software?</h2>

<p>Reivindicações de patentes relativas a software, em patentes recentemente
concedidas, geralmente tem a forma de reivindicações de <q>sistema</q> ou
<q>dispositivo</q>, reivindicações de <q>método</q> e reivindicações de
<q>produto de programa de computador</q> ou <q>meio legível por computador</q>.
Reivindicações de sistema descrevem os elementos do sistema (que podem incluir
um ou mais computadores) como um tipo de máquina ou objeto estático.
Reivindicações de método estão em formato algorítmico. Reivindicações de meio
legível por computador tipicamente duplicam as limitações encontradas nas
reivindicações de método e sistema correspondentes na patente, mas têm o
propósito de abranger o software incorporado em um armazenamento ou em um meio de
distribuição. Reivindicações de meio legível por computador também são
utilizadas para reivindicar invenções com ênfase em estruturas de dados e
interfaces de usuários(as).</p>

<h1>Violando uma patente</h1>

<h2>O que significa <q>responsabilidade de patente</q>?</h2>

<p>Responsabilidade de patente é uma responsabilidade legal que é imposta por um
tribunal. Neste documento, nós usamos a expressão <q>responsabilidade de
patente</q> para abranger os despachos que um tribunal pode emitir se uma das
partes violar uma patente. Por exemplo, uma vez que uma parte viola uma
patente, um tribunal pode ordenar que esta parte pague dinheiro ao(a) detentor(a)
da patente, o que denomina-se <q>indenização</q>, e/ou pode ordenar a
interrupção da conduta de violação, o que é chamado de <q>injunção</q>.</p>

<h2>O que significa <q>violar</q> uma patente?</h2>

<p>Violação de patente significa praticar uma ou mais de suas revindicações sem
uma licença. Se alguém usa, produz, vende, oferece para produção, oferece para
venda ou importa software que realiza qualquer elemento descrito por uma
reivindicação de uma patente, esta patente está sendo violada pelo software.</p>

<p>É possível ser responsabilizado(a) por violação de patente sem violar
diretamente. <q>Contribuir para</q> ou <q>induzir</q> violação também resultam
em responsabilidade de patente.</p>

<h2>O que é indução de violação?</h2>

<p><q>Indução de violação</q> significa encorajar ativamente alguém a violar uma
patente. A responsabilidade pela violação requer a comprovação de que a parte
incriminada pretendia que terceiros causassem a violação. Adicionalmente, quem
induz também deve saber que a patente existe, ou presumir fortemente sua
existência, e fazer esforços para não saber. Se, por exemplo, uma documentação é
redigida por alguém com conhecimento de uma reivindicação de patente, e esta
documentação explica como utilizar o programa de um modo que viole tal
revindicação, as instruções podem ser consideradas indutoras de violação. Uma
comunidade de voluntários(as) que mantém um pacote de software, e a respectiva
documentação, não podem induzir violação, a menos que os(as) voluntários(as) que
produziram a documentação conheçam a patente que supostamente foi violada.</p>

<h2>O que é violação por contribuição?</h2>

<p><q>Violação por contribuição</q> significa fornecer auxílio para a violação
de uma patente. No contexto do software, isto significaria fornecer software
não violador que poderia ser combinado com outros softwares ou hardwares para
produzir um sistema violador. A violação por contribuição também requer
conhecimento sobre a patente violada. Além disso, se o software possui uso
substancial não violador, fornecê-lo não configura violação por contribuição,
mesmo que ele seja subsequentemente utilizado em uma combinação violadora.</p>

<h2>Quais são as consequências de se violar uma patente?</h2>

<p>Se uma parte é encontrada violando uma patente, os tribunais podem ordenar a
interrupção da conduta de violação, o pagamento de indenizações pelas violações
cometidas, ou ambos. Neste documento, nós usamos a expressão <q>responsabilidade
de patente</q> para abranger todas essas consequências.</p>

<h2>O que é uma injunção?</h2>

<p>Uma injunção é uma ordem do tribunal para uma ou várias pessoas, para que
façam algo ou para que deixem de fazer algo. A violação de uma injunção resulta
em uma desobediência ao tribunal. Injunções podem ser <q>preliminares</q>, para
prevenir mudança de situação enquanto o litígio esteja ocorrendo, ou
<q>permanente</q>, para ordenar ou proibir condutas como correção ao fim do
processo legal, uma vez que a responsabilidade foi definida. Uma injunção
preliminar para prevenir condutas de violação durante o litígio pode ser
ordenada se o tribunal entender que as indenizações ao fim do caso poderiam ser
insuficientes para proteger o(a) detentor(a) dos direitos de patente, e se o
sucesso no caso for provável, e o interesse público não for prejudicado pela
injunção. Uma injunção permanente para prevenir uma conduta de violação pode
resultar de responsabilidade por violação.</p>

<h2>As injunções podem ser ordenadas contra distribuições FOSS?</h2>

<p>Sim. Se uma distribuição FOSS foi considerada violadora de uma patente válida
de alguém, uma injunção permanente poderia ocorrer contra a continuidade da
violação do programa ou funcionalidade pela distribuição.</p>

<p>Não é muito provável, entretanto, que tal injunção previna o lançamento de
toda a distribuição, ou mesmo de um pacote inteiro. É mais provável que uma
funcionalidade ou um conjunto de funcionalidades tenham que ser desabilitadas,
modificadas  de modo que o software pare a violação, ou que seja removida
inteiramente, no país em que a violação de patente ocorreu.</p>

<p>Além disso, criações que contornem as reivindicações de patente em disputa
podem prevenir até mesmo uma ou mais funcionalidades de serem removidas. Uma vez
que um elemento da reivindicação de patente não esteja mais sendo praticado,
como já dissemos, a reivindicação de patente não é mais violada. No litígio de
patentes nos Estados Unidos, o momento crucial de definição ocorre no que é
chamado de <q>audição <em>Markman</em></q>, ao fim da qual um tribunal dá uma
sentença definitiva sobre o que a reivindicação de patente em questão significa
para os propósitos daquela ação legal. Uma vez que a audição <em>Markman</em>
aconteceu, e o escopo das reivindicações declaradas tenham sido restringidas e
conclusivamente definidas, torna-se muito mais fácil fazer criações por
contorno.</p>

<h2>O que são indenizações?</h2>

<p>Na legislação de patentes, indenizações são dinheiro recebido pelo
tribunal para os(as) reclamantes quando o réu foi considerado responsável pela
violação de patente. Enquanto a lei não prevê indenizações máximas para violação
de patente, ela prevê um mínimo -- os royalties razoáveis pelo uso feito da
invenção pelo violador. Adicionalmente, o tribunal pode aumentar as indenizações
em até três vezes a indenização atual, nos casos de violação intencional.</p>

<h2>O que é violação intencional?</h2>

<p>A violação é intencional se quem infringe conhecia a patente, a menos que
o(a) violador(a) tinha, em boa fé, acreditado que a patente era inválida, ou que
sua conduta não era violadora. O(A) detentor(a) da patente deve apresentar todos
os elementos de intencionalidade, e nos tribunais dos Estados Unidos deve
fazê-lo com provas de alta qualidade, o que é denominado de <q>provas claras e
convincentes</q>.</p>

<h2>Eu não tinha conhecimento prévio de uma patente, eu posso ser considerado(a) responsável?</h2>

<p>O conhecimento da patente não é em geral requerido se a parte é acusada de
violação direta. Para ser responsável por induzir ou contribuir em uma violação,
como já foi dito, o conhecimento da patente ou esforços específicos para evitar
conhecer a patente são requeridos.</p>

<p>Na prática, detentores(as) de patentes geralmente requisitam que os(as)
infratores obtenham licenças. Se a parte obtém a licença oferecida, o(a)
detentor(a) recebe os royalties sem processá-la. Se a parte recusar a
licença, o(a) detentor(a) da patente já a colocou sob aviso e portanto está
em uma posição de reclamar a violação intencional, o que resulta em maiores
indenizações e na possibilidade de ressarcimento dos honorários advocatícios.
É possível, mas não certo, que antes que qualquer distribuição baseada na
comunidade seja processada por violação de patente, ela receberá pelo menos uma
carta demandando que a licença seja obtida.</p>

<h2>E se a violação foi acidental, inadvertida ou não intencional?</h2>

<p>Uma violação não intencional ou inadvertida não pode ser intencional, como
dissemos acima. Como também não se pode contribuir ou induzir uma violação
acidentalmente, já que conhecimento e intenção são ambos necessários. Mas alguém
pode ser responsabilizado por violação direta, sem conhecimento ou
intenção, pelo uso ou venda ou fabricação ou causando a criação de software
infrator.</p>

<h2>Como fico sabendo da existência de uma patente?</h2>

<p>Existem inúmeros caminhos para se ficar ciente da existência de uma patente
específica. Além de ser diretamente contatado(a) pelo(a) detentor(a) da patente,
você pode conhecer sobre uma patente particular através de uma busca pela web ou
por listas de discussão, ou em conexão com seu emprego, etc. Se ficar ciente de
uma patente que lhe cause preocupação, é melhor que você converse com um(a)
advogado(a) em vez de compartilhar tal conhecimento ou especulação em um fórum
público.</p>

<h2>Quais são as defesas disponíveis em uma ação de violação de patente?</h2>

<p>Primeiro, pode haver defesas específicas para os fatos e circunstâncias da
situação em particular, e é trabalho do(a) advogado(a) detectar e desenvolver
essas defesas. Algumas defesas estão ou podem estar presentes na maioria dos
casos, incluindo:</p>

<p>Permissão: Você não é responsável por violação se você tem permissão para
usar as reivindicações. Tal permissão pode ser explícita. Uma permissão
explícita é chamada de <q>licença</q>. Permissões também podem ser implícitas:
elas podem resultar de condutas ou declarações do(a) detentor(a) da patente
que se apresentam como uma permissão e nas quais você confiou. (Advogados(as)
chamam isto de <q>preclusão (estoppel)</q>). Pode também resultar de uma total
inação pelo(a) detentor(a) da patente, que efetivamente pode permitir a conduta
violadora porque <q>o direito não socorre os(as) que dormem</q>, o que os(as)
advogados(as) denominam de <q>decadência (laches)</q>.</p>

<p>Não violação: Uma determinação de não violação é a demonstração de que, na
verdade, nenhuma das reivindicações de patente <q>se apresentavam</q> no
software em questão. Em outras palavras, o software na verdade não implementa
cada elemento que está descrito em qualquer reivindicação.</p>

<p>Invalidade: Se a patente é inválida, ela não pode ser violada. A invalidade
pode ser demonstrada ao se provar que a matéria em questão da patente está fora
do escopo da legislação de patente. Ela também pode ser apresentada pela
demonstração, sob a lei dos Estados Unidos, de que a patente é <q>não nova</q>
ou <q>óbvia</q>. Sob a legislação de patentes, para que a patente seja válida,
a invenção reivindicada deve ter sido útil, redutível à prática, nova e não
óbvia para uma <q>pessoa que possua habilidade comum naquele ofício</q> no
período em que a invenção tenha sido feita. Uma defesa de invalidade, portanto,
mostra que a patente falhou em cumprir alguns desses requerimentos.</p>

<h1>O risco de patente para uma distribuição de comunidade</h1>

<h2>Você pode fornecer exemplos de violação de patente contra comunidades FOSS?</h2>

<p>Não. Felizmente, poucos casos existem e ainda nenhum chegou a um julgamento
final. Até a presente data, nenhum tribunal abordou grande parte das temáticas
que são únicas às distribuições de software livre. Nós acreditamos que isto
ocorra porque as comunidades FOSS não têm <q>muitos recursos</q> para que
royalties sejam pagos, e processar desenvolvedores(as) individuais que não
possuem grandes receitas resulta em má publicidade para detentores(as) de
patentes sem que se alcance qualquer efeito útil.</p>

<h2>Nós somos uma distribuição FOSS e não ganhamos dinheiro. Como pagaremos as indenizações se isto no for imposto?</h2>

<p>Esta questão, como todas as questões similares sobre os riscos legais e
responsabilidades dos projetos, dependem muito dos detalhes de sua estrutura
legal e de suas relações comerciais. Não há uma resposta genérica sobre como os
projetos lidam com seus riscos legais, incluindo os riscos de sentenças
indenizatórias por violação de patente ou outras responsabilidades.
O <a href="http://www.softwarefreedom.org">SFLC</a>,
a <a href="http://sfconservancy.org/">Software Freedom Conservancy</a>,
a <a href="http://www.apache.org/">Apache Software Foundation</a>,
a <a href="http://www.fsf.org">Free Software Foundation</a>,
a <a href="https://www.spi-inc.org/">Software in the Public Interest</a> e
outras organizações ajudam os projetos a se enquadrarem dentro de contextos
legais e organizações que podem, de forma útil, abordar essas questões em um
nível mais geral. Se sua distribuição ou um projeto dentro de sua distribuição
acredita que enfrentará potenciais responsabilidades legais, você deve nos
consultar, ou consultar uma das outras organizações acima.</p>

<h2>Nós somos uma distribuição FOSS e ganhamos dinheiro. Isto faz com que sejamos mais suscetíveis a ações legais de violação de patente?</h2>

<p>Qualquer pessoa que obtenha receitas é um alvo mais atraente para ser
processado(a) por um(a) detentor(a) de patente do que alguém que não ganha
dinheiro e que não possa pagar indenizações. Uma distribuição de comunidade que
absolutamente não tem receita não é um alvo atraente. Mas mesmo se você ganhar
algumas centenas de milhares de dólares por ano em vendas, comparado com uma
empresa lucrativa do tamanho da Microsoft, ou mesmo com a Red Hat, você não vale
o custo do litígio para um troll de patente ou outro reclamante racional.</p>

<h2>Eu ouvi falar que distribuir código-fonte é mais seguro do que distribuir código-objeto. Isto é verdade?</h2>

<p>Sim. Distribuir código-fonte é provavelmente mais seguro do que distribuir
binários, devido a algumas razões. Primeiro, o código-fonte, como as próprias
publicações de patente, ensinam como a invenção funciona, em vez de ser a
própria invenção. Se o código-fonte sozinho pode violar a patente, é difícil de
entender como a distribuição de fotocópias da patente mesma não seriam
violações. Segundo, nos Estados Unidos, os tribunais <em>podem</em> entender o
código-fonte como discurso, como acreditamos que eles devem entender, então
fazendo com que o código-fonte esteja sujeito à proteção da Primeira Emenda dos
Estados Unidos. Nós sabemos pouco sobre como a Suprema Corte harmonizaria a lei
de patentes com as demandas da Primeira Emenda. Nós do SFLC escrevemos diversos
dossiês sobre a Suprema Corte abordar essas matérias, mas a Corte nunca chegou,
nessas matérias ou decidiu sobre elas. Além disso, como mencionado acima, a
responsabilidade pela violação de patente pode ser imposta quando alguém
possibilita ou encoraja outra pessoa a violar uma patente, mas os requisitos
de conhecimento e intenção são mais estritos em situações de responsabilidade
secundária. Como o(a) usuário(a) primeiro tem que compilar o código-fonte e
instalar o software para que ocorra a violação, o tribunal tem menos chances de
responsabilizar a comunidade por indução ou contribuição de violação.</p>

<h2>Como parte de um projeto de distribuição de comunidade, quem é a pessoa mais provável de ser processada por violação de patente?</h2>

<p>Este é um problema para o(a) agressor(a) potencial da patente, mais do que
para a distribuição. Uma distribuição de comunidade composta por
voluntários(as), sem qualquer estrutura hierárquica trabalhista ou de
supervisão, não pode ser processada ao se processar <q>a cabeça</q> do projeto.
Se a violação requer intenção e conhecimento, ou esforços específicos para não
conhecer, como ocorre nos casos de indução ou contribuição à violação, o
indivíduo com tal intenção ou conhecimento deve provavelmente ser encontrado(a)
e processado(a) pessoalmente. Se a pessoa que escreve código e documentação não
lê patentes, e os(as) voluntários(as) que desenvolvem código para um pacote não
mantêm o mesmo pacote ou pacotes relacionados, o(a) agressor(a) pode encontrar
dificuldades para processar alguém.</p>

<p>As especificidades de uma dada situação, contudo, sem dúvida serão cruciais.
Como todas as outras matérias do tipo, se você acredita que uma patente será
reclamada contra sua distribuição ou seus(as) voluntários(as), você deve
contatar o SFLC ou outro(a) advogado(a) imediatamente.</p>

<h2>Você está sugerindo que é melhor que desenvolvedores(as) e contribuidores(as) não leiam as patentes? Se sim, por quê?</h2>

<p>Sim. Infelizmente, a lei de patentes dos Estados Unidos cria desincentivos
para a busca por patentes, até mesmo porque uma das justificativas dadas para o
sistema de patentes é que a patente ensina ao público como praticar uma invenção
que, de outro modo, seria secreta. A violação <q>intencional</q> sujeita o(a)
infrator(a) a maiores indenizações quando eles(as) estão cientes da patente e
pretendiam violá-la, e ler as patentes aumenta a probabilidade que a violação
subsequente seja entendida como intencional. Além disso, nós sabemos que os(as)
desenvolvedores(as) pressupõem que as patentes que eles descobrem são mais
amplas em escopo do que realmente são, e então tais desenvolvedores(as)
tornam-se excessivamente ou desnecessariamente preocupados(as). Se, apesar
disso, você pretende conduzir uma busca por uma patente, você primeiro deve
procurar aconselhamento jurídico.</p>

<h2>Eu estou fora dos Estados Unidos. Tem alguma coisa com que devo me preocupar?</h2>

<p>Embora a maior parte dos países sejam membros da Organização Mundial da
Propriedade Intelectual (WIPO - World Intellectual Property Organization),
como também signatários do Tratado de Cooperação de Patente (PCT - Patent
Cooperation Treaty), grandes corporações geralmente restringem suas atividades
de aquisições de patente nos <q>Três Grandes</q>: Estados Unidos, Europa e
Japão. Isto é considerado proteção suficiente pela maioria das companhias,
embora as companhias tenham crescentemente declarado requisições de patentes na
China com a esperança de que direitos de patente serão, eventualmente,
respeitadas de forma adequada pelo respectivo governo e comunidades de negócios.
Além disso, grandes corporações multinacionais em outras jurisdições, como
Coreia e Canadá, geralmente apresentarão o pedido de solicitação de patente em
seu próprio país antes de apresentar a solicitação de patente
internacionalmente. Na Índia, alguns softwares têm sido patenteados apesar da
clara declaração normativa de que software <em>per se</em> não é patenteável. O
SFLC na Índia já iniciou a contestação dessa patentes.</p>

<p>Mas independentemente de onde você trabalha, o software que viola patentes
não pode ser importado para os países onde essas patentes foram emitidas, o que
significa que você no mínimo deveria estar preocupado(a) com a competência de
alcançar seus(as) pretendidos(as) usuários(as).</p>

<p>Como sempre, a consulta a um advogado(a) local é um bom passo se você tiver
qualquer questão sobre sua situação ou responsabilidades.</p>

<h2>Existem diretrizes para limitar nosso risco de violação de patente?</h2>

<p>Sim. Este documento tem o objetivo de educar sobre o risco de patente e,
embora seja difícil dar conselhos sobre cada situação específica de cada
leitor(a), existem algumas diretrizes que podem ser extraídas.</p>

<ul>
<li><p>Ler as patentes, especialmente quando se pesquisa sobre como projetar uma
contribuição para seu projeto de software livre, pode expor as comunidades à
responsabilidade que elas, em outras circunstâncias, não teriam.</p></li>
<li><p>Partes de uma comunidade de software livre que distribuem código-fonte e
não código-objeto provavelmente terão menor chances de risco de patente.</p></li>
<li><p>Distribuir software livre comercialmente é provavelmente mais arriscado
do que distribuir software de graça.</p></li>
<li><p>Ter a capacidade de remover funcionalidades e pacotes das distribuições,
rapidamente e com facilidade, ajudará a mitigar os danos que a comunidade possa
sofrer.</p></li>
<li><p>O litígio de patente não é um esporte amador. Se você for contatado por
alguém que lhe ameaçou a imposição de patente, contate o Software Freedom Law
Center ou outro(a) advogado(a) qualificado(a) o mais rapidamente possível.</p></li>
</ul>

<hr />

<p><strong>Agradecimentos.</strong> Este documento foi preparado pelos(as)
advogados(as) do SFLC, com contribuições de Stefano Zacchiroli, em nome do
Projeto Debian.</p>
