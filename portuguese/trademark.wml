#use wml::debian::template title="Marcas registradas do Debian"
#use wml::debian::toc
#use wml::debian::translation-check translation="7877b46be9028804be6ca61d385c4aeecc91c653" maintainer="Daniel Lenharo de Souza (lenharo)"

<toc-display />


<toc-add-entry name="trademarks">Marcas registradas</toc-add-entry>

<p>O Debian está empenhado em proteger e garantir o uso consistente de sua
marca, logos e estilos, e também tornar mais fácil o uso para todo(a) o(a) usuário(a)
<em>genuíno(a)</em>.
Como parte desse processo, a marca Debian é uma marca registrada nos Estados
Unidos pela <a href="https://www.spi-inc.org/corporate/trademarks/">Software
in the Public Interest, Inc.</a>, gerenciada pelo projeto Debian. Para
<em>registro fora dos Estados Unidos</em>, nós aderimos a aplicação do
<a href="https://en.wikipedia.org/wiki/Madrid_system">Protocolo de Madri</a>
para estender a proteção para a União Europeia, China e Japão; a marca Debian
também está registrada
<a href="https://gru.inpi.gov.br/pePI/servlet/MarcasServletController?Action=searchMarca&amp;tipoPesquisa=BY_NUM_PROC&amp;Source=OAMI&amp;NumPedido=827045310">no Brasil</a>,
com número do processo 827045310, mas ela está expirada.</p>

<p>A marca Debian foi primeiro
<a href="http://tarr.uspto.gov/servlet/tarr?regser=serial&amp;\
entry=75386376">registrada</a> em 21 de dezembro de 1999, mas tem sido usada
desde agosto de 1993. Ela está registrada sob a Classe Nice 009:\
 <q>Utilitário Computacional e Software de Sistema Operacional</q>.</p>


<toc-add-entry name="policy">Política de uso da marca</toc-add-entry>

<pre><code>Versão: 2.0
Publicado: 19 de janeiro de 2013
</code></pre>

<p>A <a href="https://www.spi-inc.org/">Software in the Public Interest, Inc.</a>
detém o registro de várias marcas, tanto na forma de palavras quanto de
logotipo, incluindo marcas, slogans e estilos. Esta política abrange todas as
marcas, em formato de palavras e logotipo, coletivamente chamadas de
<q>marcas registradas do Debian</q>. Você pode ver uma lista não exaustiva de
marcas registradas do Debian, incluindo marcas registradas e não registradas
(mas de outra forma legalmente reconhecidas) em nossa página de
<a href="#trademarks">marcas registradas</a>.</p>

<p>O objetico desta política de uso da marca é:</p>

<ol>
   <li>Incentivar o uso e adoção generalizada das marcas registradas do Debian,</li>
   <li>Esclarecer o uso adequado por terceiros das marcas registradas do Debian,</li>
   <li>Evitar o uso indevido das marcas registradas do Debian que podem
     confundir ou enganar os(as) usuários(as) com relação ao Debian ou suas
     afiliadas.</li>
</ol>

<p>Por favor, observe que não é o objetivo desta política limitar a atividade
comercial em torno do Debian. Incentivamos comerciantes a trabalharem com
Debian, enquanto estiverem em conformidade com esta política.</p>

<p>A seguir, estão as diretrizes para o uso adequado das marcas registradas
do Debian por editores e outros interessados.
Qualquer uso ou referência as marcas registradas do Debian que seja inconsistente com estas diretrizes,
ou outro uso não autorizado ou referência às marcas registradas do Debian,
ou uso de marcas que sejam confundidamente semelhantes às marcas registradasdo Debian,
é proibido e pode violar os direitos da marca registrada do Debian.</p>

<p>Qualquer uso das marcas registradas do Debian de maneira enganosa e falsa, ou que
deprecie o Debian, como propaganda enganosa, é sempre proibido.</p>


<h3>Quando você pode usar as marcas registradas do Debian sem precisar pedir permissão</h3>

<ol>

  <li>Você pode usar as marcas registrada do Debian para fazer declarações
    factuais verdadeiras sobre o Debian ou comunicar a compatibilidade com
     seu produto com sinceridade.</li>

  <li>Seu uso pretendido é qualificado como <q>uso justo nominativo</q> das
     marcas registradas Debian, ou seja, apenas identificando que você está
     falando sobre o Debian em um texto, sem sugerir patrocínio ou endosso.</li>

  <li>Você pode usar as marcas registradas do Debian para descrever ou anunciar
    seus serviços ou produtos relacionados ao Debian de uma maneira que não
    seja enganosa.</li>

  <li>Você pode usar as marcas registradas do Debian para descrever o Debian
    em artigos, títulos ou postagens de blog.</li>

  <li>Você pode fazer camisetas, papéis de parede para desktops, bonés ou outras
    mercadorias com marcas registradas do Debian para
    <em>uso não comercial</em>.</li>

  <li>Você também pode fazer mercadorias com as marcas registradas do Debian
    para <em>uso comercial</em>. No caso de uso comercial, recomendamos que
    você anuncie com sinceridade aos clientes que parte do preço de venda,
    se houver, será doada ao projeto Debian. Veja nossa
    <a href="$(HOME)/donations"> página de doações </a> para mais
    informações sobre como doar para o projeto Debian.</li>

</ol>


<h3>Quando você NUNCA pode usar as marcas registradas do Debian sem pedir permissão</h3>

<ol>

  <li>Você não pode usar as marcas registradas do Debian de qualquer
      maneira que sugira uma afiliação ou endosso pelo projeto ou pela
      comunidade Debian, se isso não for verdade.</li>

  <li>Você não pode usar as marcas registradas do Debian no nome de
    empresa ou organização ou como nome de um produto ou serviço.</li>

  <li>Você não pode usar um nome que seja confundidamente semelhante as
    marcas registradas do Debian.</li>

  <li>Você não pode usar as marcas registradas do Debian no nome de
    domínio, com ou sem propósito comercial.</li>

</ol>


<h3>Como utilizar as marcas registradas do Debian</h3>

<ol>

  <li>Use as marcas registradas do Debian de uma maneira que deixe claro que seu
   projeto está relacionado ao projeto Debian, mas que não faz parte do
   Debian, produzido pelo projeto Debian ou endossado pelo projeto Debian.
   </li>

  <li>Reconheça a Software in the Public Interest, Inc. como proprietária
    da marca Debian com destaque.

     <p><em>Exemplo:</em></p>

     <p>[TRADEMARK] é uma (<q>registrada,</q> se aplicável) marca registrada de
     propriedade da Software in the Public Interest, Inc.</p>
  </li>

  <li>Insira um aviso sobre isenção de responsabilidade sobre patrocínio,
    afiliação, ou endosso pelo Debian em seu site e em todos os materiais
    impressos relacionados.

      <p><em>Exemplo:</em></p>

      <p>O PROJETO X não é afiliado ao Debian. Debian é uma marca registrada
      de propriedade da Sofware in the Public Interest, Inc.</p>
    </li>

  <li>Faça distinção das marcas registradas do Debian das palavras próximas,
    colocando em itálico, negrito ou sublinhando. </li>

  <li>Use as marcas registradas do Debian em sua forma exata, sem abreviar, sem hifenizar,
    e sem combinar com qualquer outra palavra ou palavras. </li>

  <li>Não crie siglas utilizando as marcas registradas do Debian. </li>

</ol>

<h3>Permissão para uso</h3>

<p>Em caso de dúvida sobre o uso das marcas registradas do Debian, ou para solicitar
permissão para usos não permitidos por esta política, envie um email (em inglês) para
<a href="mailto:trademark@debian.org?subject=Trademark%20Use%20Request">\
trademark@debian.org com o assunto <q>Solicitação de uso de marca registrada\
</q></a>; tenha certeza de incluir as seguintes informações no corpo da sua
mensagem: </p>
<ul>
<li>Nome do(a) usuário(a)</li>
<li>Nome da organização/projeto </li>
<li>Finalidade de uso (comercial/não comercial)</li>
<li>Natureza do uso</li>
</ul>

<h3>Versões mais recentes desta política</h3>

<p>Esta política pode ser revisada periodicamente e as versões atualizadas
 ficarão disponíveis em <url https://www.debian.org/trademark>.</p>

<h3>Diretrizes para o uso dos logotipos</h3>

<ul>

   <li>Qualquer redimensionamento deve manter as proporções originais do
     logotipo.</li>

   <li>Não use os logotipos do Debian como parte do logotipo da sua empresa,
     produto ou marca própria. Eles podem ser usados como parte de uma página
     que descreve seu produto ou serviço.</li>

   <li>Você não precisa nos pedir permissão para usar os logotipos em seu
     próprio site apenas como um link para o site do projeto Debian.</li>

</ul>

<p>
Para qualquer dúvida sobre essas diretrizes, envie um email (em inglês) para
<email "trademark@debian.org">.
</p>

<toc-add-entry name="licenses">Organizações licenciadas para usarem as marcas registadas do Debian</toc-add-entry>

<p>As seguintes organizações foram licenciadas para usarem as marcas registadas do Debian através
de um acordo de licenciamento:</p>

<ul>

<li>Stiftelsen SLX Debian Labs, por resolução da SPI
<a href="https://www.spi-inc.org/corporate/resolutions/2004/2004-01-05.bmh.1/">\
2004-01-05.bmh.1</a>.</li>

<li>Vincent Renardias e <q>Les logiciels du soleil</q>, por resolução da SPI
<a href="https://www.spi-inc.org/corporate/resolutions/1999/1999-08-06.mgs/">\
1999-08-06.mgs</a>.</li>

<li>
debian.ch, <a
href="https://lists.debian.org/debian-www/2011/04/msg00163.html">\
por Stefano Zacchiroli</a>, líder do Projeto Debian.
</li>

</ul>
