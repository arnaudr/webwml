#use wml::debian::template title="Debian BTS &mdash; informações para desenvolvedores(as)" NOHEADER=yes NOCOPYRIGHT=true
#include "$(ENGLISHDIR)/Bugs/pkgreport-opts.inc"
#use wml::debian::translation-check translation="204e82ebfe88f1243e0f7d9a424fd41a034b7f9b" maintainer="Thiago Pezzo (Tico)"

<h1>Informações sobre o sistema de processamento de bugs para mantenedores(as)
de pacotes e classificadores(as) de bugs</h1>

<p>Inicialmente, um relatório de bug é enviado por um(a) usuário(a) como
uma mensagem de e-mail comum para <code>submit@bugs.debian.org</code>, o qual
deve incluir uma linha <code>Package</code> (para mais informações veja
<a href="Reporting">instruções para relatar bug</a>). Um número é então
atribuído ao relatório, informado ao(à) usuário(a) e encaminhado para
<code>debian-bugs-dist</code>. Se a linha <code>Package</code> contiver
um pacote que tenha um(a) ou mais mantenedores(as) conhecidos(a), eles(as) também
receberão uma cópia.</p>

<p>Na campo <code>Assunto</code> será adicionado
<code>Bug#</code><var>nnn</var><code>:</code> e o campo <code>Responder para</code>
incluirá o(a) usuário(a) que enviou o relatório e também
<var>nnn</var><code>@bugs.debian.org</code>.</p>

<ul class="toc">
  <li><a href="#closing">Fechando relatórios de bugs</a></li>
  <li><a href="#followup">Mensagens de acompanhamento</a></li>
  <li><a href="#severities">Níveis de severidade</a></li>
  <li><a href="#tags">Tags para relatórios de bugs</a></li>
  <li><a href="#forward">Registrando que você encaminhou um relatório de bug</a></li>
  <li><a href="#owner">Alterando o responsável pelo bug</a></li>
  <li><a href="#maintincorrect">Mantenedores(as) de pacotes listados(as) incorretamente</a></li>
  <li><a href="#requestserv">Reabrindo, reatribuindo e manipulando bugs</a></li>
  <li><a href="#subscribe">Inscrevendo-se em um bug</a></li>
  <li><a href="#subjectscan">Recurso mais ou menos obsoleto de procura por assunto</a></li>
  <li><a href="#x-debian-pr">Recurso obsoleto <code>X-Debian-PR: quiet</code></a></li>
</ul>

<h2><a name="closing">Fechando relatórios de bugs</a></h2>

<p>Os relatórios de bugs Debian devem ser fechados quando o problema é
corrigido. Problemas em pacotes só podem ser considerados corrigidos apenas
quando um pacote que inclui a correção para o bug entra no repositório
Debian.</p>

<p>Normalmente, as únicas pessoas que devem fechar um relatório
de bug são: o(a) usuário(a) que relatou o bug e os(as) mantenedores(as)
do pacote para o qual o bug foi enviado. Existem exceções para
esta regra, por exemplo, os bugs relatados para pacotes desconhecidos ou para
certos pseudopacotes genéricos. Um bug também pode ser fechado por qualquer
contribuidor(a) se o bug for de um pacote <strong>órfão</strong>, ou se o(a)
mantenedor(a) do pacote esqueceu de fechá-lo. É muito importante mencionar a
versão do pacote para a qual o bug foi corrigido. Em caso de dúvida, não feche
o bug, peça primeiro por ajuda na lista de discussão debian-devel.</p>

<p>Relatórios de bugs devem ser fechados através do envio de um
e-mail para <var>nnn</var><code>-done@bugs.debian.org</code>.
O corpo da mensagem precisa conter uma explicação de como o bug
foi corrigido.</p>

<p>Com as mensagens recebidas do sistema de acompanhamento de bugs, tudo
o que você precisa fazer para fechar o bug é enviar uma resposta através de
seu programa de e-mail e editar o campo <code>Para</code> inserindo
<var>nnn</var><code>-done@bugs.debian.org</code> em vez de
<var>nnn</var><code>@bugs.debian.org</code>
(<var>nnn</var><code>-close</code> é fornecido como um apelido para
<var>nnn</var><code>-done</code>).</p>

<p>Onde aplicável, por favor forneça uma linha <code>Version</code>
no <a href="Reporting#pseudoheader">pseudocabeçalho</a>
de sua mensagem quando fechar um bug, de forma que o sistema de acompanhamento
de bugs saiba quais versões do pacote contêm a correção.</p>

<p>A pessoa que está fechando o bug, a pessoa que o relatou e a lista de
discussão <code>debian-bugs-closed</code> irão todos receber uma notificação
sobre a mudança do status do relatório. A pessoa que enviou o relatório e a
lista de discussão também receberão o conteúdo da mensagem enviada
para <var>nnn</var><code>-done</code>.</p>


<h2><a name="followup">Mensagens de acompanhamento</a></h2>

<p>O sistema de acompanhamento de bugs incluirá o endereço da pessoa que
relatou o bug e o endereço do bug (<var>nnn</var><code>@bugs.debian.org</code>)
no cabeçalho <code>Responder para</code> depois de encaminhar o relatório do
bug. Por favor, observe que estes são dois endereços distintos.</p>

<p>
Qualquer desenvolvedor(a) que deseje responder a um relatório de bugs deve
simplesmente responder à mensagem, respeitando o cabeçalho
<code>Responder para</code>. Isto <strong>não</strong> fechará o bug.</p>

<p><em>Não</em> use as funcionalidades <q>responder para todos</q> ou
<q>encaminhar</q> do seu leitor de e-mail, a não ser que você deseje editar os
destinatários consideravelmente. Em particular, não envie
mensagens encaminhadas para <code>submit@bugs.debian.org</code>.</p>

<p>
As mensagens podem ser enviadas para os seguintes endereços a fim de serem
arquivadas no sistema de acompanhamento de bugs:
</p>

<ul>
<li>
<var>nnn</var><code>@bugs.debian.org</code> - estas mensagens também são enviadas
para o(a) mantenedor(a) do pacote e encaminhadas para
<code>debian-bugs-dist</code>, mas <strong>não</strong> para a pessoa que
relatou o bug;
</li>
<li>
<var>nnn</var><code>-submitter@bugs.debian.org</code> - estas também são enviadas
para a pessoa que relatou o bug e encaminhadas para <code>debian-bugs-dist</code>,
mas <strong>não</strong> para o(a) mantenedor(a) do pacote;
</li>
<li>
<var>nnn</var><code>-maintonly@bugs.debian.org</code> - estas são enviados somente
para o(a) mantenedor(a) do pacote, e <strong>não</strong> para a pessoa que
relatou o bug ou <code>debian-bugs-dist</code>;
</li>
<li>
<var>nnn</var><code>-quiet@bugs.debian.org</code> - estas são apenas arquivadas
no sistema de acompanhamento de bug (como todos os anteriores), e
<strong>não</strong> são enviadas para mais ninguém.
</li>
</ul>

<p>Para mais informações sobre cabeçalhos para suprimir mensagens de
confirmação e como enviar com cópias usando o sistema de acompanhamento
de bugs, veja as <a href="Reporting">instruções para relatar bugs</a>.</p>


<h2><a name="severities">Níveis de severidade</a></h2>

<p>O sistema de acompanhamento de bugs grava um nível de severidade para
cada relatório de bug. Este é definido como <code>normal</code> por padrão, mas
pode ser sobrescrito incluindo uma linha <code>Severity</code> no
pseudocabeçalho quando o bug é enviado (consulte as
<a href="Reporting#pseudoheader">instruções para reportar bugs</a>), ou usando
o comando <code>severity</code> com o <a href="#requestserv">servidor de
requisição de controle</a>.</p>

<p>Os níveis de severidade são:</p>

<dl>
<dt><code>critical</code></dt>
<dd>faz com que o software não relacionado ao sistema (ou o sistema
todo) pare, ou causa sérias perdas de dados, ou introduz uma falha de
segurança nos sistemas onde você instala o pacote.</dd>

<dt><code>grave</code></dt>
<dd>torna o pacote em questão inutilizável ou quase inutilizável, ou causa
perda de dados, ou introduz uma falha de segurança, permitindo acesso às
contas dos(as) usuários(as) que utilizam o pacote.</dd>

<dt><code>serious</code></dt>
<dd>é uma <a href="$(DOC)/debian-policy/">violação
severa da política Debian</a> (praticamente, viola uma diretiva
<q>must</q> ou <q>required</q>, ou, na opinião do(a) mantenedor(a) do pacote ou
do(a) gerente de lançamento, torna o pacote impróprio para o lançamento.</dd>

<dt><code>important</code></dt>
<dd>um bug que tem um efeito maior na utilização de um pacote, sem
torná-lo completamente inutilizável para todos(as).</dd>

<dt><code>normal</code></dt>
<dd>o valor padrão, aplicável à maioria dos bugs.</dd>

<dt><code>minor</code></dt>
<dd>um problema que não afeta a utilidade do pacote, e de correção
presumivelmente trivial.</dd>

<dt><code>wishlist</code></dt>
<dd>para requisição de qualquer melhoria, e também para quaisquer bugs que sejam
muito difíceis de corrigir devido a grandes considerações de design.</dd>
</dl>

<p>Certas severidades são consideradas
<em><a href="https://bugs.debian.org/release-critical/">release-critical</a></em>,
significando que o bug terá um impacto na liberação do pacote na
versão estável (stable) do Debian. Atualmente, as severidades
nesta categoria são <strong>critical</strong>, <strong>grave</strong> e
<strong>serious</strong>. Para regras completas e canônicas sobre quais
problemas merecem tais severidades, veja a lista dos
<a href="https://release.debian.org/testing/rc_policy.txt">problemas
release-critical para o próximo lançamento</a></p>

<h2><a name="tags">Tags para relatórios de bugs</a></h2>

<p>Cada bug pode ter zero ou mais conjunto de tags. Estas tags são
exibidas na lista de bugs quando você consulta a página do pacote e quando
você consulta o registro completo do bug.</p>

<p>As tags podem ser definidas incluindo uma linha <code>Tags</code> no
pseudocabeçalho quando o bug é reportado (consulte as
<a href="Reporting#pseudoheader">instruções para reportar bugs</a>)
ou usando o comando <code>tags</code> com o
<a href="#requestserv">servidor de requisição de controle</a>. Separe
tags múltiplas com vírgulas, espaços ou ambos.</p>

<p>As tags atuais dos bugs são: <bts_tags>. Aqui estão algumas
 informações detalhadas sobre as tags:</p>

<dl>

<dt><code>patch</code></dt>
  <dd>Um patch (correção) ou outro procedimento fácil para corrigir o bug é
  incluído nos registros do bug. Caso exista um patch, mas o mesmo não corrija o
  bug adequadamente ou cause outros problemas, esta tag não deve ser usada.</dd>

<dt><code>wontfix</code></dt>
  <dd>Este bug não será corrigido. Possivelmente porque esta é uma
  escolha entre duas maneiras arbitrárias de fazer as coisas, e o(a)
  mantenedor(a) e a pessoa que relatou o bug preferem maneiras diferentes
  de fazer as coisas, possivelmente porque a mudança de comportamento causará
  outros problemas piores para os outros, ou possivelmente por outras razões.</dd>

<dt><code>moreinfo</code></dt>
  <dd>Este bug não pode ser direcionado até que mais informações sejam
  fornecidas pela pessoa que relatou o mesmo. O bug será fechado caso a
  pessoa que o relatou não forneça maiores informações em um período de
  tempo razoável (alguns meses). Esta é para bugs do tipo <q>Não
  funciona</q>. O que não funciona?</dd>

<dt><code>unreproducible</code></dt>
  <dd>Este bug não pode ser reproduzido no sistema do(a) mantenedor(a). A
  assistência de terceiros é necessária para diagnosticar a causa do
  problema.</dd>

<dt><code>help</code></dt>
<dd>O(A) mantenedor(a) está requisitando ajuda para lidar com este bug. Ou o(a)
  mantenedor(a) não tem as habilidades necessárias para corrigir esse bug e
  necessita de colaboração, ou está sobrecarregado(a) e quer delegar essa
  tarefa. Este bug pode não ser adequado para novos(as) colaboradores(as) a
  menos que também esteja marcado com a tag <code>newcomer</code>.</dd>

<dt><code>newcomer</code></dt>
  <dd>Este bug tem uma solução conhecida mas o(a) mantenedor(a) solicita que
  outra pessoa o implemente. Esta é uma tarefa ideal para novos(as)
  contribuidores(as) que desejam se envolver com o Debian, ou que desejam
  melhorar suas habilidades.</dd>

<dt><code>pending</code></dt>
  <dd>Uma solução para este bug foi encontrada e um envio será feito em
  breve.</dd>
	
<dt><code>fixed</code></dt>
  <dd>Este bug foi corrigido ou contornado (por exemplo foi enviado por um(a)
  não mantenedor(a)), mas ainda existe um problema que precisa ser
  resolvido. Esta tag substitui a antiga severidade <q>fixed</q>.</dd>

<dt><code>security</code></dt>
  <dd>Este bug descreve um problema de segurança em um pacote (por
  exemplo, permissões erradas liberando o acesso a dados que não
  deveriam estar acessíveis; buffer overruns permitindo que pessoas
  controlem um sistema de uma maneira que não deveriam ser capazes
  de fazer; ataques de negação de serviço que precisam ser
  corrigidos, etc). A maioria dos bugs de segurança também devem
  ser definidos para uma severidade critical ou grave.</dd>

<dt><code>upstream</code></dt>
  <dd>Este bug aplica-se à parte do pacote sob responsabilidade do(a)
  desenvolvedor(a) original.</dd>

<dt><code>confirmed</code></dt>
  <dd>O(A) mantenedor(a) do pacote verificou, entendeu e basicamente concorda
  com o bug, mas ainda tem que consertá-lo. O uso dessa tag é opcional; ela é
  usada para aqueles(as) mantenedores(as) que precisam gerenciar uma grande
  quantidade de bugs em aberto.</dd>

<dt><code>fixed-upstream</code></dt>
  <dd>O bug foi corrigido pelo(a) desenvolvedor(a) original, mas ainda não foi
  corrigido no pacote (por uma razão qualquer: talvez seja muito complicado
  fazer a migração da mudança ou é uma mudança tão pequena que não vale a
  pena).</dd>

<dt><code>fixed-in-experimental</code></dt>
  <dd>O bug foi corrigido no pacote da versão experimental,
  mas ainda não foi corrigido na versão instável (unstable).</dd>

<dt><code>d-i</code></dt>
  <dd>Esse bug é importante para o desenvolvimento do instalador do Debian. É
  esperado que essa tag seja usada quando o bug afetar o desenvolvimento
  do instalador, mas não está associado a um pacote que faz parte do próprio
  instalador.</dd>

<dt><code>ipv6</code></dt>
  <dd>Esse bug afeta o suporte à versão 6 do Protocolo de Internet (IP).</dd>

<dt><code>lfs</code></dt>
  <dd>Esse bug afeta o suporte a arquivos grandes (maiores que 2 gigabytes)</dd>

<dt><code>l10n</code></dt>
  <dd>Este bug é relevante para a localização do pacote.</dd>

<dt><code>a11y</code></dt>
  <dd>Este bug é relevante para a acessibilidade do pacote.</dd>

<dt><code>ftbfs</code></dt>
  <dd>O pacote falha ao ser construído a partir do código-fonte. Se o bug foi atribuído
    para um pacote-fonte, aquele pacote falha na construção. Se o bug foi
    atribuído para um pacote binário, os pacotes-fonte afetados falham na
    construção. A tag é aplicável para ambientes de construção não
    padrões (por exemplo, usando Build-Depends da experimental), mas a
    severidade deve ser abaixo de serious (release critical) nesses casos.</dd>

<dt><bts_release_tags></dt>
  <dd>Estas são tags de versão, que possuem dois efeitos. Quando
    colocada em um bug, o bug pode afetar apenas aquela versão em particular
    (embora ele também possa afetar outras versões se outras tags de
    versão foram colocadas), caso contrário são aplicadas regras normais
    de problemático/corrigido/inexistente. O bug também não deve ser arquivado
    até que seja corrigido na versão.</dd>

<dt><bts_release_ignore_tags></dt>
  <dd>Este bug release-critical é para ser ignorado, permitindo, assim, o
  lançamento dessa versão em particular. <strong> Estas tags devem ser usadas
  somente pelo(s)/pela(s) gerente(s) de lançamento; não defina isso sozinho sem
  a autorização explícita dele(s)/dela(s).</strong></dd>

</dl>

<p>Algumas informações sobre tags específicas de versão:
  As tags -ignore ignoram o bug para permitir a propagação para a
  teste (testing). As tags de versões indicam que o bug em
  questão não deve ser arquivado antes de ser corrigido no conjunto das
  versões marcadas. As tags de versão também indicam que um bug
  somente pode ser considerado problemático em um conjunto de versões
  específicas. [Em outras palavras, se qualquer das tags de versão estão
  inseridas, o bug é <strong>inexistente</strong> em qualquer versão cuja tag
  daquela versão correspondente <strong>não</strong> está inserida.
  Caso contrário, as regras normais de encontrado/corrigido se aplicam.]
</p>

<p>
  Tags de versões <strong>não</strong> devem ser usadas se o versionamento
  adequado do bug conseguir o efeito desejado, uma vez que exigem a adição e
  a remoção manual. Se você não tem certeza se uma tag de versão é
  necessária, entre em contato com os(as) administradores(as) do Debian BTS
  (<email "owner@bugs.debian.org">) ou o time de versão para esclarecimentos.
</p>

<h2><a name="forward">Registrando que você encaminhou um relatório de bug</a></h2>

<p>Quando um(a) desenvolvedor(a) encaminha um relatório de bugs para o(a)
desenvolvedor(a) original do código fonte do qual o pacote Debian é
derivado, deve-se anotar no sistema de acompanhamento de
bugs como abaixo:</p>

<p>Certifique-se de que o campo <code>Para</code> da sua
mensagem para o(a) autor(a) possua apenas o endereço de e-email dele(a) (ou os
endeços de e-mail no caso de ser mais de um(a) autor(a)); no campo
<code>Cc</code> coloque: a pessoa que reportou o bug,
<var>nnn</var><code>-forwarded@bugs.debian.org</code> e
<var>nnn</var><code>@bugs.debian.org</code>.</p>

<p>Peça ao(à) autor(a) para preservar o campo <code>Cc</code>
deixando <var>nnn</var><code>-forwarded@bugs.debian.org</code> quando responder,
assim o sistema de acompanhamento de bugs irá arquivar a resposta com o
relatório original. Estas mensagens são apenas arquivadas e não
enviadas; para enviar uma mensagem da forma normal, envie-a para
<var>nnn</var><code>@bugs.debian.org</code>.</p>

<p>Quando o sistema de acompanhamento de bugs recebe uma mensagem com
<var>nnn</var><code>-forwarded</code>, ele marca o bug relevante como tendo sido
encaminhado para o(s) endereço(s) no campo <code>Para</code> da mensagem que ele
recebe, caso o bug já não esteja marcado como encaminhado.</p>

<p>Você pode também manipular a informação <q>forwarded to</q> enviando
mensagens para
<a href="server-control"><code>control@bugs.debian.org</code></a>.</p>


<h2><a name="owner">Alterando o responsável pelo bug</a></h2>

<p>Nos casos onde a pessoa responsável por consertar o bug não é
o(a) mantenedor(a) do pacote (por exemplo, quando o pacote é
mantido por uma equipe), pode ser útil registrar esse
fato no sistema de processamento de bugs. Para facilitar, cada bug
pode opcionalmente ter um(a) dono(a).</p>

<p>O(A) dono(a) pode ser configurado(a) adicionando a linha <code>Owner</code>
no pseudocabeçalho quando o bug é enviado (veja as
<a href="Reporting#pseudoheader">instruções para relatar bugs</a>),
ou usando os comandos <code>owner</code> e <code>noowner</code> no
<a href="#requestserv">servidor de requisição de controle</a>.</p>


<h2><a name="maintincorrect">Mantenedores(as) de pacotes listados(as) incorretamente</a></h2>

<p>Se o(a) mantenedor(a) de um pacote está listado(a) incorretamente,
é porque, geralmente, o(a) mantenedor(a) mudou recentemente e
o(a) novo(a) mantenedor(a) não fez o envio de uma nova versão do
pacote com um campo <code>Maintainer</code> do arquivo de controle modificado.
Isto será corrigido quando o pacote for enviado; alternativamente, os(as)
mantenedores(as) do repositório podem sobrescrever o registro do(a)
mantenedor(a) de um pacote manualmente, por exemplo, se uma recompilação e um
reenvio do pacote não são esperados tão cedo.
Entre em contato com <code>override-change@debian.org</code> para mudanças no
arquivo de sobrescrita (override file).</p>


<h2><a name="requestserv">Reabrindo, reatribuindo e manipulando bugs</a></h2>

<p>É possível reatribuir relatórios de bugs para outros pacotes,
reabrir bugs fechados erroneamente, modificar a informação dizendo
para onde, caso exista, um relatório de bug foi encaminhado, mudar
as severidades e títulos de relatórios, configurar o(a) responsável pelos
bugs, juntar e separar relatórios de bugs e registrar as
versões dos pacotes nos quais os bugs foram encontrados e em quais delas os
bugs foram corrigidos. Isto é feito enviando e-mails para
<code>control@bugs.debian.org</code>.</p>

<p>O <a href="server-control">formato destas mensagens</a> é descrito em
outro documento disponível no web site, ou no arquivo
<code>bug-maint-mailcontrol.txt</code>. Uma versão em texto puro
também pode ser obtida enviando uma mensagem com a palavra
<code>help</code> para o servidor no endereço acima.</p>

<h2><a name="subscribe">Inscrevendo-se em bugs</a></h2>

<p>O sistema de acompanhamento de bugs também permite que a pessoa que relatou o
bug, os(as) desenvolvedores(as) e qualquer outra parte interessada, inscrevam-se
em um bug específico. Esse recurso pode ser usado por aqueles(as) que desejam
monitorar um bug, sem que seja necessário inscrever-se em um pacote através do
<a href="https://tracker.debian.org">rastreador de pacotes do Debian</a>. Todas
as mensagens recebidas em <var>nnn</var><code>@bugs.debian.org</code>, são
enviadas aos(às) inscritos(as).</p>

<p>A inscrição em um bug pode ser feita através do envio de um e-mail para
<var>nnn</var><code>-subscribe@bugs.debian.org</code>. O assunto e o conteúdo
do e-mail são ignorados pelo BTS. Uma vez que a mensagem seja processada, os(as)
usuários(as) receberão um e-mail de confirmação que deve ser respondido para que
eles(as) comecem a receber as mensagens relacionadas ao bug.</p>

<p>Também é possível cancelar a inscrição de um bug. O cancelamento da
inscrição pode ser feita através do envio de um e-mail para
<var>nnn</var><code>-unsubscribe@bugs.debian.org</code>. O
assunto e o conteúdo do e-mail são novamente ignorados pelo BTS. Os(As)
usuários(as) receberão uma mensagem de confirmação que deve ser respondida para
que eles(as) tenham as inscrições do bug canceladas.</p>

<p>Por padrão, o endereço inscrito é aquele encontrado no campo <code>De</code>
do cabeçalho da mensagem. Se você deseja inscrever um outro
endereço de e-mail em um bug, você terá que codificar o endereço a ser inscrito dentro
da mensagem de inscrição. Eis o formato utilizado para isso:
<var>nnn</var><code>-subscribe-</code>\
<var>localpart</var><code>=</code>\
<var>example.com</var><code>@bugs.debian.org</code>.
Esse exemplo enviaria à <code>localpart@example.com</code> uma mensagem de
inscrição no bug <var>nnn</var>. O símbolo <code>@</code> deve ser codificado
mudando para o símbolo <code>=</code>.
Similarmente, um cancelamento de inscrição utilizaria o seguinte formato:
<var>nnn</var><code>-unsubscribe-</code><var>localpart</var>\
<code>=</code><var>example.com</var><code>@bugs.debian.org</code>.
Em ambos os casos, o assunto e o corpo do e-mail serão encaminhados ao
endereço contido na requisição de confirmação.</p>

<h2><a name="subjectscan">Recurso mais ou menos obsoleto de procura por assunto</a></h2>

<p>Mensagens que chegam em <code>submit</code> ou <code>bugs</code>
cujo campo assunto inicie com <code>Bug#</code><var>nnn</var> serão tratadas
como tendo sido enviadas para <var>nnn</var><code>@bugs.debian.org</code>.
Isto é para compatibilidade retroativa com mensagens encaminhadas a partir do
endereço antigo e para pegar mensagens enviadas para <code>submit</code> por
engano (por exemplo, usando "responder para todos os destinatários").</p>

<p>Um esquema similar opera para <code>maintonly</code>,
<code>done</code>, <code>quiet</code> e  <code>forwarded</code>,
os quais tratam mensagens que chegam com uma tag Subject como tendo sido
enviadas para o endereço <var>nnn-qualquercoisa</var><code>@bugs.debian.org</code>
correspondente.</p>

<p>Mensagens que chegam com <code>forwarded</code> puro e <code>done</code> &mdash;
isto é, sem o número do relatório de bugs no endereço &mdash; e sem um número de
bug no campo assunto serão arquivadas como <q>junk</q> e mantidas por algumas
semanas, mas por outro lado serão ignoradas.</p>


<h2><a name="x-debian-pr">Recurso obsoleto <code>X-Debian-PR: quiet</code></a></h2>

<p>É usado para ser possível prevenir que o sistema de acompanhamento
de bugs encaminhe para qualquer lugar mensagens que o mesmo recebeu em
<code>debian-bugs</code>, colocando uma linha
<code>X-Debian-PR: quiet</code> no cabeçalho da mensagem atual.</p>

<p>Esta linha de cabeçalho é agora ignorada. Em vez disso, envie
sua mensagem para <code>quiet</code> ou <var>nnn</var><code>-quiet</code>
(ou <code>maintonly</code> ou <var>nnn</var><code>-maintonly</code>).</p>

<hr />

#use "otherpages.inc"

#use "$(ENGLISHDIR)/Bugs/footer.inc"
