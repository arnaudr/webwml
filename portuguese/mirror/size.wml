#use wml::debian::template title="Tamanho do espelho"
#use wml::debian::translation-check translation="2952c49b0944458062097885874441a4245a870b"

<h2>Qual é o tamanho do repositório Debian?</h2>

# (note for the English editors on how to update some of the numbers below)
# dak psql database on ftp-master is 'projectb'
# and there's a copy on merkel
# projectb=> select architecture.arch_string as Architecture,
#            sum(files.size)/1024/1024/1024 as Size
#            from files,binaries,architecture
#            where architecture.id=binaries.architecture
#            and files.id=binaries.file
#            group by architecture.arch_string
#            order by Size;
# projectb=> select sum(size)/1024/1024/1024 from files where
#            filename ~ '.diff.gz$' or filename ~ '.dsc$'
#            or filename ~ '.orig.tar.gz$';
# projectb=> select sum(files.size)/1024/1024/1024
#            from files, binaries, architecture
#            where architecture.id=binaries.architecture
#            and files.id=binaries.file
#            and architecture.arch_string='i386';

# wc -c'ing files inside the debian/ directory might occasionally give
# slightly different results than the SQL queries, but the difference
# is usually negligible -joy


<p>Os números nesta página são atualizados diariamente.

<table>
<tr><th>Arquitetura</th>  <th>Tamanho em GB</th></tr>
#include "$(ENGLISHDIR)/mirror/size.data"
</table>

<p>Note que o repositório está constantemente em crescimento; a teste
(<q>testing</q>) crescerá especialmente quando seu lançamento se aproximar.
Também não recomendamos excluir distribuições específicas para reduzir o tamanho
de um espelho; em vez disso, exclua arquiteturas específicas
baseado na sua <a href="https://popcon.debian.org/">popularidade</a>.</p>

<h3>Qual é o tamanho do repositório Debian CD?</h3>

<p>O repositório de CDs varia muito entre os espelhos &mdash; os arquivos
Jigdo têm por volta de 100-150 MB por arquitetura, enquanto as imagens
completas de DVD/CD têm em torno de 15 GB cada, mais o espaço extra para
imagens de CD de atualização (<q>update</q>), arquivos Bittorrent, etc.</p>

<p>Para mais informações sobre espelhamento do repositório de CD, por favor,
veja as <a href="../CD/mirroring/">páginas sobre espelhos Debian CD</a>.</p>

<h2>Qual é o tamanho das atualizações regulares dos espelhos?</h2>

<p>Para o repositório <q>main</q> do Debian, por favor, veja
<a href="https://ftp-master.debian.org/size-quarter.png">o gráfico do
tamanho da execução diária</a>.</p>

