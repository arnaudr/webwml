#use wml::debian::template title="Het gebruik van WML"
#use wml::debian::translation-check translation="24a8bbc5bf2fd2fbe025f0baa536bf1126f83723"

<p>WML staat voor <q>web site meta language</q> (metataal voor webpagina's). Dit
betekent dat WML invoer krijgt uit .wml-bestanden, dat het alles wat zich daarin
ook maar bevindt (dit kan alles zijn van basale HTML tot Perl-code!), verwerkt en
dat het elke gewenste uitvoer genereert, bijvoorbeeld .html of .php.</p>

<p>Het is niet eenvoudig om WML te leren vanuit de documentatie. Deze is
eigenlijk wel behoorlijk volledig, maar totdat u begint te begrijpen hoe WML
werkt (en het is behoorlijk krachtig), is het makkelijker om het te leren,
vertrekkend vanuit voorbeelden. Mogelijk vindt u de sjabloonbestanden die voor
de Debian-site worden gebruikt, nuttig. Deze zijn te vinden in
<code><a href="https://salsa.debian.org/webmaster-team/webwml/tree/master/english/template/debian">\
webwml/english/template/debian/</a></code>.</p>

<p>Hier gaan we ervan uit dat u WML geïnstalleerd heeft op uw computer. Het is
beschikbaar als
<a href="https://packages.debian.org/wml">Debian pakket/a>.


<h2>WML-broncode bewerken</h2>

<p>Eén ding zullen alle .wml-bestanden hebben, en dat is één of meer
inleidende <code>#use</code>-regels. Hun syntaxis moet u niet wijzigen of
vertalen, enkel de tekstfragmenten tussen aanhalingstekens, zoals die na
<code>title=</code>, waardoor in de uitvoerbestanden het &lt;title&gt;-element
aangepast wordt.</p>

<p>Op de koptekstregels na, bevatten de meeste van onze .wml-pagina's eenvoudige
HTML. Indien u tags tegenkomt, zoals &lt;define-tag&gt; of &lt;: ... :&gt;,
ga daar dan voorzichtig mee om, omdat dit begrenzingen zijn van code die
verwerkt wordt door een van de bijzondere cyclussen van WML. Zie hieronder voor
meer informatie.</p>


<h2>Het bouwen van Debian webpagina's</h2>

<p>Type gewoon <kbd>make</kbd> in webwml/&lt;taal&gt;. We hebben make-bestanden
aangemaakt welke <kbd>wml</kbd> aanroepen met al de passende argumenten.</p>

<p>Indien u het commando <kbd>make install</kbd> ingeeft, zullen de
HTML-bestanden gebouwd worden en in de map <kbd>../../www/</kbd> geplaatst
 worden.</p>


<h2>Extra WML-functies die we gebruiken</h2>

<p>Een van de functies van WML waarvan we uitgebreid gebruik maken, is het
gebruik van Perl. Onthoud dat dit geen dynamische pagina's zijn. Perl wordt
gebruikt op het ogenblik van het bouwen van de HTML om, nu ja, alles wat u maar
wilt, te doen. Twee goede voorbeelden van de wijze waarop we Perl gebruiken, is
voor het creëren van de lijst van meest recente nieuws-items voor de startpagina
en voor het genereren van de links naar de vertalingen aan het eind van de
pagina.

# TODO: add the basic stuff from webwml/english/po/README here

<p>Om de sjablonen van onze website opnieuw op te bouwen heeft u versie
&gt;= 2.0.6 van wml nodig. Om de gettext-sjablonen voor niet-Engelstalige
vertalingen opnieuw te bouwen is mp4h &gt;= 1.3.0 nodig.</p>


<h2>Specifieke problemen met WML</h2>

<p>Voor multibyte-talen kan een speciale voor- of nabewerking van de
.wml-bestanden nodig zijn om de tekenset correct te kunnen verwerken. Dit kan
men doen door in <kbd>webwml/&lt;lang&gt;/Make.lang</kbd> op gepaste wijze de
variabelen <kbd>WMLPROLOG</kbd> en <kbd>WMLEPILOG</kbd> aan te passen.
Afhankelijk van hoe uw <kbd>WMLEPILOG</kbd>-programma werkt, kan het zijn dat u
de waarde van kbd>WMLOUTFILE</kbd> moet aanpassen.
<br>
Zie de vertalingen naar het Japans of het Chinees voor een voorbeeld.
</p>
