#use wml::debian::template title="데비안 구하기"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="50283624a3deec87adfe5a87644c4aad93d91c53" maintainer="Seunghun Han (kkamagui)"

<p>데비안은 인터넷을 통해 <a href="../intro/free">자유롭게</a>
배포됩니다.
여러분은 어느 <a href="ftplist">미러 서버</a>에서든 전부 다운로드할 수 있죠.
<a href="../releases/stable/installmanual">설치 매뉴얼</a>에는 자세한 설치 과정이
들어 있고, 릴리스 노트는
<a href="../releases/stable/releasenotes">여기</a>에서 찾을 수 있습니다.
</p>

<p>여러분이 데비안을 쉽게 설치하고 싶다면, 아래의 선택지가 있습니다:</p>

<div class="line">
  <div class="item col50">
    <h2><a href="netinst">설치 이미지 다운로드</a></h2>
    <p>인터넷 속도에 따라, 아래 항목에서 하나를 다운로드하기 바랍니다:</p>
    <ul>
      <li><a href="netinst"><strong>작은 설치 이미지</strong></a>:
      빨리 다운로드할 수 있고 이동식 디스크에 기록되어야 합니다.
      작은 설치 이미지를 이용하려면 인터넷에 연결된 컴퓨터가 필요합니다.
	<ul class="quicklist downlist">
	  <li><a title="64비트 Intel 및 AMD PC를 위한 설치관리자 다운로드"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">64비트 PC용 네트워크 설치 ISO</a></li>
	  <li><a title="일반 32비트 Intel 및 AMD PC를 위한 설치관리자 다운로드"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">32비트 PC용 네트워크 설치 ISO</a></li>
	</ul>
      </li>
      <li>더 큰 <a href="../CD/"><strong>완전한 설치 이미지</strong></a>: 
      더 많은 패키지가 들어있고, 인터넷 연결 없이 더 쉽게 설치할 수 있습니다.
	<ul class="quicklist downlist">
	  <li><a title="64비트 Intel 및 AMD PC를 위한 DVD 토런트 다운로드"
	         href="<stable-images-url/>/amd64/bt-dvd/">64비트 PC용 토런트 (DVD)</a></li>
	  <li><a title="일반 32비트 Intel 및 AMD PC를 위한 DVD 토런트 다운로드 "
		 href="<stable-images-url/>/i386/bt-dvd/">32비트 PC용 토런트 (DVD)</a></li>
	  <li><a title="64비트 Intel 및 AMD PC를 위한 CD 토런트 다운로드"
	         href="<stable-images-url/>/amd64/bt-cd/">64비트 PC용 토런트 (CD)</a></li>
	  <li><a title="일반 32비트 Intel과 AMD PC를 위한 CD 토런트 다운로드"
		 href="<stable-images-url/>/i386/bt-cd/">32비트 PC용 토런트 (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
  <h2><a href="https://cloud.debian.org/images/cloud/">데비안 클라우드 이미지를 쓰세요</a></h2>
    <ul>
      <li>공식 <a href="https://cloud.debian.org/images/cloud/"><strong>
      클라우드 이미지</strong></a>:
      여러분의 클라우드 서비스 제공자가 바로 사용할 수 있고,
      데비안 클라우드 팀이 만듭니다.
        <ul class="quicklist downlist">
          <li><a title="64비트 Intel 및 AMD를 위한 오픈스택(OpenStack) Qcow2 이미지"
           href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-amd64.qcow2">64비트 AMD/Intel용 오픈스택(OpenStack) (Qcow2)</a></li>
          <li><a title="64비트 ARM을 위한 오픈스택(OpenStack) Qcow2 이미지"
          href="https://cloud.debian.org/cdimage/openstack/current-10/debian-10-openstack-arm64.qcow2">64비트 ARM용 오픈스택(OpenStack) (Qcow2)</a></li>
        </ul>
      </li>
    </ul>

    <h2><a href="../CD/live/">설치 전에 데비안 라이브를 쓰세요</a></h2>
    <p>CD, DVD, USB 키에서 라이브 시스템을 부팅하면 컴퓨터에 파일을
    설치하지 않고 데비안을 써 볼 수 있습니다.
    여러분이 마음에 준비가 되면, 라이브 시스템에 들어있는 설치 프로그램을
    실행할 수도 있죠.
    데비안 라이브 이미지가 여러분의 크기, 언어, 패키지 선택에 관한 요구사항을
    만족한다면 이 방법이 적합할 수 있습니다.
    <a href="../CD/live#choose_live">데비안 라이브를 사용하는 방법 대한 정보</a>를
    좀 더 읽으면 결정하는데 도움이 될 겁니다.
    </p>
    <ul class="quicklist downlist">
      <li><a title="64비트 Intel 및 AMD PC를 위한 토런트 다운로드"
	     href="<live-images-url/>/amd64/bt-hybrid/">64비트 Intel 및 AMD PC용 라이브 토런트</a></li>
      <li><a title="32비트 Intel 및 AMD PC를 위한 토런트 다운로드"
	     href="<live-images-url/>/i386/bt-hybrid/">32비트 Intel 및 AMD PC용 라이브 토런트</a></li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2><a href="../CD/vendors/">데비안 CD를 파는 공급 업체에서 CD나 DVD 세트 구매</a></h2>

   <p>많은 공급업체들이 배포판을 5달러 이하의 가격에 배송비를 더해 팝니다.
   (웹 페이지에서 국제 배용 여부 확인).
      <br />
   몇몇 <a href="../doc/books">데비안 관련 책</a>에도 CD가 동봉되어 있죠.
   </p>

   <p>CD의 기본적인 장점은 아래와 같습니다:</p>

   <ul>
     <li>CD 세트로 설치하는 게 더 간단합니다.</li>
     <li>인터넷에 연결하지 않고 컴퓨터에 설치할 수 있습니다.</li>
     <li>데비안을 여러분이 원하는 만큼 많은 컴퓨터에 설치할 수 있습니다.
     여러분이 직접 모든 패키지를 다운로드하지 않고도 말이죠.</li>
     <li>CD는 손상된 데비안 시스템을 더 쉽게 복구할 수 있습니다.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">데비안이 설치된 컴퓨터 구매</a></h2>
   <p>컴퓨터 구매는 여러 장점이 있습니다:</p>
   <ul>
    <li>데비안 설치 안 해도 됩니다.</li>
    <li>하드웨어에 맞게 미리 구성되어 설치됩니다.</li>
    <li>공급업체가 기술지원을 할 수도 있습니다.</li>
   </ul>
  </div>
</div>
