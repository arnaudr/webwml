#use wml::debian::ddp title="DDP:n kehittäjän oppaat"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"
#use wml::debian::translation-check translation="e58910f188b6d887c1e0310e961ee8e79d21888c"

<document "Debianin linjan kuvaus (englanninkielinen)" "policy">

<div class="centerblock">
<p>
  Tämä opas kertoo Debian GNU/Linux-jakelun linjausten vaatimuksista.
  Niihin kuuluu Debian-arkiston rakenne ja sisältö, useita käyttöjärjestelmän
  suunnitteluun liittyviä asioita sekä myös paketilta vaadittavat tekniset
  ominaisuudet, jotta se hyväksytään mukaan jakeluun.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "debian-policy">

  <p><a href="debian-policy/">HTML-versio</a>,
     <a href="debian-policy/policy.pdf.gz">PDF-versio</a>,
     <a href="debian-policy/policy.ps.gz">PS-versio</a>,
     <a href="debian-policy/policy.txt.gz">tekstiversio</a>

  <br>
  <a href="https://bugs.debian.org/debian-policy">Linjaan ehdotetut parannukset</a>

  <p>Hae SGML-lähdeteksti
  <a href="https://anonscm.debian.org/cgit/dbnpolicy/policy.git/">debian-policy</a>-paketille
  <a href="https://packages.debian.org/git-core">Git</a>illa.

  <p>
  Hae lähdekoodit komennolla:
  </p>
  <pre>git clone git://git.debian.org/git/dbnpolicy/policy.git</pre>

  <p>Lisäosia toimintalinjauksiin:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Tiedostojärjestelmän rakenteen standardi (FHS)</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">tekstiversio</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Pakettipäivitysten tarkistuslista</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Näennäispakettien nimilista</a>
    <li><a href="packaging-manuals/menu-policy/">Menu-linja</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">tekstiversio</a>]
    <li><a href="packaging-manuals/perl-policy/">Perl-linja</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">tekstiversio</a>]
    <li><a href="packaging-manuals/debconf_specification.html">debconf-määrittelyt</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacsen-linja</a>
    <li><a href="packaging-manuals/java-policy/">Java-linja</a>
    <li><a href="packaging-manuals/python-policy/">Python-linja</a>
    <li><a href="packaging-manuals/copyrigh-format/1.0/">tekijänoikeus-muodon määrittelyt</a>
  </ul>
</availability>
</doctable>
</div>

<hr>

<document "Debian-kehittäjien käsikirja (englanninkielinen)" "devref">

<div class="centerblock">
<p>
  Tämä opas käsittelee Debian-ylläpitäjien toimintatapoja ja resursseja.
  Se kertoo uudeksi kehittäjäksi ryhtymisestä, tiedostojen lähetysmenetelmästä,
  vianseurantajärjestelmän käsittelystä, postilistoista, palvelimistamme jne.

  <p>Tämä opas on ajateltu <em>käsikirjaksi</em> kaikille Debian-kehittäjille
  (uusille ja vanhoille guruille).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Adam Di Carlo, Rapha&euml;l Hertzog, Josip Rodin">
  <maintainer "Adam Di Carlo, Rapha&euml;l Hertzog, Josip Rodin">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Uuden Debian-ylläpitäjän opas (englanninkielinen)" "maint-guide">

<div class="centerblock">
<p>
  Tämä dokumentti pyrkii kuvailemaan Debian GNU/Linux-paketin rakentamista
  tavalliselle Debian-käyttäjälle (ja kehittäjäksi haluavalle) yleiskielellä,
  ja sisältää toimivia esimerkkejä.

  <p>Toisin kuin aikaisemmat yritykset, tämä pohjautuu
  <code>debhelper</code>:iin ja uusiin kehittäjien käytössä oleviin
  työkaluihin.  Kirjoittaja tekee parhaansa yrittäessään sisällyttää
  mukaan aikaisemmat teokset.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Johdatus Debian-paketointiin" "packaging-tutorial">

<div class="centerblock">
<p>
Tämä lyhytkurssi tarjoaa johdatuksen Debian-paketointiin.
Se opettaa tulevia kehittäjiä kuinka muokata nykyisiä paketteja,
kuinka luoda heidän omia paketteja ja kuinka olla yhteyksissä
Debian-yhteisön kanssa.
Tärkeimmän lyhytkurssin lisäksi se sisältää kolme käytännön
harjoitusta: <code>grep</code>-paketin muokkaamisen,
<code>gnujump</code>-pelin ja Java-kirjaston paketoimisen.
</p>

<doctable>
 <authors "Lucas Nussbaum">
 <maintainer "Lucas Nussbaum">
 <status>
 ready
 </status>
 <availability>
 <inpackage "packaging-tutorial">
 <inddpvcs-packaging-tutorial>
 <p>
 Nouda TeX-lähdekoodi <a
 href="https://packages.debian.org/git-core">Git</a>llä osoitteesta
 <a href="https://anonscm.debian.org/cgit/collab-maint/packaging-tutorial.git">packaging-tutorial</a>.
 </p>

 <p>
 Lähdekoodin checkout tapahtuu komennolla:
 </p>
 <pre>git clone git://git.debian.org/collab-maint/packaging-tutorial.git</pre>

 </availability>
</doctable>
</div>

<hr>

<document "Debian-valikkojärjestelmä (englanninkielinen)" "menu">

<div class="centerblock">
<p>
  Tämä opas käsittelee Debianin valikkojärjestelmää ja
  <strong>menu</strong>-pakettia.

  <p>menu-paketti syntyi vanhan fvwm2-paketin install-fvwm2-menu-ohjelman
  inspiroimana.  menu yrittää kuitenkin tarjota yleismallisemman liittymän
  valikoiden rakentamiseen.  Käyttämällä tämän paketin update-menus-komentoa,
  paketteja ei tarvitse muokata erikseen jokaista X-ikkunamanageria varten,
  ja se tarjoaa yhteisen liittymän sekä tekstitilan että graafisille ohjelmille.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML-versio</a>
  </availability>
</doctable>
</div>



